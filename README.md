# Xand Banks
**Table of Contents**
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Getting Started](#getting-started)
- [Usage](#usage)
- [Testing](#testing)
- [Money handling](#money-handling)
  - [Usd](#usd)
  - [Conversions](#conversions)
  - [Value Comparisons](#value-comparisons)
  - [Arithmetic operations](#arithmetic-operations)
  - [Error handling](#error-handling)
- [Implementation Details](#implementation-details)
  - [Bank memo field limitations](#bank-memo-field-limitations)
    - [Provident](#provident)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Getting Started

This crate contains implementation details for each supported bank, a bank builder to build the banks from their 
config files, and a bank dispatcher to route requests to the correct bank based on routing number.

## Usage

When adding a new bank for use by the dispatcher it must implement the Bank trait. This ensures that it provides 
all of the necessary functionality for use in the XAND ecosystem.

External crates can provide their own Bank implementations for use with the dispatcher as long as their corresponding 
config files implement FromConfig. This allows the bank builder to build the banks and get them to the dispatcher.

Currently supported banks: 
    * MCB
    * Provident (via Treasury Prime)

## Testing

All of the unit tests can be run using `cargo test`.

Integration tests can be run with `make integ-tests` Note that this requires you to have docker setup on your machine and be logged into GCR.

## Implementation Details

### Monetary operations

This crate uses `xand_money` and its `Usd` type to perform monetary operations on fiat claims.
See the [xand_money docs](https://gitlab.com/TransparentIncDevelopment/product/libs/xand_money) for more info. 

### Bank memo field limitations

| Bank Name   | Memo field character limit | Supported characters |
|-------------|----------------------------|----------------------|
| Provident   | 1kb serialized json        | Any utf-8?           |
| MCB         | 33                         | ?                    |
| Silvergate  | 40                         | ?                    |

#### Provident

We integrate with the Provident bank backend via the [Treasury Prime API](https://dashboard.treasuryprime.com/docs) using Basic Auth (found in 1Password).
The TreasuryPrime API is a surface that implements many banks, including Provident. 

Testing is performed against `api.sandbox.treasuryprime.com`.

We use a generated Swagger OpenApi 2 Rust client as an interface between `xand-banks` and the Treasury API client.
The generated client is checked into source control.
See the [Generate Clients](../generated/README.md) Readme for more info.

**High level architecture**

![](TPFS_Provident_TP_Integration_v2-24-2020.jpg)

A "create" is represented by the gray dot. 
A "send" across the XAND network is represented by azure dots. 
A "redeem" is represented by green dots.

For issues concerning Treasury Prime API integration, reach out to a TPFS engineer for access to their Slack or email the [Treasury Prime helpdesk](mailto:help@treasuryprime.com).
