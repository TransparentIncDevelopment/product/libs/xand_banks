pub mod account;
pub mod bank;

use derive_new::new;
use serde::*;
use std::str::FromStr;
use xand_money::Usd;

#[derive(Clone, Debug, Deserialize, PartialEq, Eq, Serialize, Default)]
#[serde(rename_all = "kebab-case")]
pub struct BankAccount {
    pub id: String,
    pub display_name: String,
    pub account_number: String,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Eq, Serialize, Default)]
#[serde(rename_all = "kebab-case")]
/// A BankAccount wrapped with its associated bank's routing number
pub struct EnrichedBankAccount {
    pub id: String,
    pub display_name: String,
    pub account_number: String,
    pub routing_number: String,
    pub bank_name: String,
}

/// Represents the relevant parts of a bank transaction.
#[derive(Debug, Clone, new, PartialEq, Eq)]
pub struct BankTransaction {
    pub bank_unique_id: String,
    pub amount: Usd,
    pub metadata: String,
    pub txn_type: BankTransactionType,
}

/// Enum to differentiate transactions in which the account received money vs sent money
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum BankTransactionType {
    /// Money leaving the account
    Debit,
    /// Money coming into the account
    Credit,
}

impl FromStr for BankTransactionType {
    type Err = ();

    fn from_str(s: &str) -> std::result::Result<BankTransactionType, ()> {
        match s {
            "Debit" => Ok(BankTransactionType::Debit),
            "Credit" => Ok(BankTransactionType::Credit),
            _ => Err(()),
        }
    }
}

/// The available balance and current balance (in USD) of a bank account.
#[derive(Debug, Clone, Default, PartialEq, Eq, new)]
pub struct BankBalance {
    pub available_balance: Usd,
    pub current_balance: Usd,
}

#[derive(Debug, Clone, Default, new)]
pub struct BankTransferResponse {
    pub message: String,
}
