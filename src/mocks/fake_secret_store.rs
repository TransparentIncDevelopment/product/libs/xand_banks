use async_trait::async_trait;
use futures::lock::Mutex;
use std::collections::HashMap;
use xand_secrets::{CheckHealthError, ReadSecretError, Secret, SecretKeyValueStore};

pub struct FakeSecretStore {
    store: Mutex<HashMap<String, String>>,
}

impl FakeSecretStore {
    pub fn new() -> Self {
        FakeSecretStore {
            store: Mutex::new(HashMap::new()),
        }
    }

    pub async fn add_secret(&self, key: &str, secret: &str) {
        let mut secret_store = self.store.lock().await;
        secret_store.insert(String::from(key), String::from(secret));
    }
}

impl Default for FakeSecretStore {
    fn default() -> Self {
        FakeSecretStore::new()
    }
}

#[async_trait]
impl SecretKeyValueStore for FakeSecretStore {
    async fn read(&self, key: &str) -> Result<Secret<String>, ReadSecretError> {
        let secret_store = self.store.lock().await;
        let secret_value = secret_store.get(key).ok_or(ReadSecretError::KeyNotFound {
            key: key.to_owned(),
        })?;

        let secret = Secret::new(secret_value.to_owned());

        Ok(secret)
    }

    async fn check_health(&self) -> Result<(), CheckHealthError> {
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mocks::fake_secret_store::FakeSecretStore;
    use matches::assert_matches;
    use xand_secrets::{ExposeSecret, ReadSecretError, SecretKeyValueStore};

    #[tokio::test]
    async fn read_secret_using_api_call() -> Result<(), ReadSecretError> {
        let secret_store = FakeSecretStore::default();
        let key = "my/custom/path/myKey";
        let secret = "mySecret";
        secret_store.add_secret(key, secret).await;

        let result = secret_store.read(key).await?;

        assert_eq!(secret, result.expose_secret());

        Ok(())
    }

    #[tokio::test]
    async fn key_not_found_error() {
        let secret_store = FakeSecretStore::default();
        let my_key = "my/custom/path/myKey";
        let result = secret_store.read(my_key).await;
        if let Err(ReadSecretError::KeyNotFound { key }) = result {
            assert_eq!(my_key, key.as_str());
        } else {
            panic!("Expected KeyNotFoundError, got {:?}", result);
        }
    }

    #[tokio::test]
    async fn always_healthy() {
        let secret_store = FakeSecretStore::default();
        let result = secret_store.check_health().await;
        assert_matches!(result, Ok(()));
    }
}
