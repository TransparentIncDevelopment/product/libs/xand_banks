use crate::errors::*;
use crate::Result;
use chrono::{DateTime, Duration, Utc};

#[derive(Debug, Clone)]
pub struct DateRange {
    begin_date: DateTime<Utc>,
    end_date: DateTime<Utc>,
}

impl DateRange {
    /// Return a date range where end_date is now, and begin_date is (now - duration)
    pub fn from_past_to_now(duration: Duration) -> Result<Self> {
        if duration.num_milliseconds() < 0 {
            return Err(XandBanksErrors::General{message: "Received negative duration for building DateRange. This will result in a begin_date after now, which is invalid. Please pass a positive Duration".to_string() });
        }

        let now = Utc::now();
        let begin_date = now - duration;
        Ok(DateRange {
            begin_date,
            end_date: now,
        })
    }

    /// Builid a date range representing the last 24 hours until now
    pub fn last_24_hours() -> Self {
        let now = Utc::now();

        let duration = Duration::hours(24);
        let begin_date = now - duration;
        DateRange {
            begin_date,
            end_date: now,
        }
    }

    /// Getter for begin_date
    pub fn begin_date(&self) -> DateTime<Utc> {
        self.begin_date
    }

    /// Getter for end_date
    pub fn end_date(&self) -> DateTime<Utc> {
        self.end_date
    }

    pub fn new(begin_date: DateTime<Utc>, end_date: DateTime<Utc>) -> Self {
        Self {
            begin_date,
            end_date,
        }
    }
}

impl Default for DateRange {
    fn default() -> Self {
        Self {
            begin_date: Utc::now(),
            end_date: Utc::now(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_build_valid_date_range() {
        let duration = Duration::hours(1);
        let _date_range = DateRange::from_past_to_now(duration).unwrap();
    }

    #[test]
    fn date_range_errors_on_negative_duration() {
        let duration = Duration::hours(-1);
        let date_range = DateRange::from_past_to_now(duration);
        assert!(date_range.is_err())
    }
}
