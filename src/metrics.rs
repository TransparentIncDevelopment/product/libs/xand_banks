use crate::{
    date_range::DateRange,
    models::{BankBalance, BankTransaction, BankTransferResponse},
    Bank, Result,
};
use lazy_static::lazy_static;
use pipesrv_emitter::spin_up_emitter;
use std::any::Any;
use xand_metrics::{DimensionedCtrDef, MetricOpts, Metrics, MetricsBag};
use xand_metrics_prometheus::PromMetrics;

use crate::models::account::{DetailedAccount, TransferRequest};
use async_trait::async_trait;

const EMIT_CADENCE_SECS: u64 = 10;

lazy_static! {
    static ref BANK_METRICS: BankMetricsBag = {
        let mut bank_metrics_bag = BankMetricsBag::default();
        let metrics = PromMetrics::default();
        metrics.initialize(&mut bank_metrics_bag).unwrap();
        spin_up_emitter(EMIT_CADENCE_SECS, Box::new(metrics));
        bank_metrics_bag
    };
}

#[derive(Clone)]
pub struct MeteredBankDecorator<T: Bank + Clone + Send + Sync> {
    bank: T,
}

impl<T: Bank + Clone + Send + Sync> MeteredBankDecorator<T> {
    pub fn new(bank: T) -> Self {
        Self { bank }
    }
}

#[async_trait]
impl<T: Bank + Clone + Send + Sync> Bank for MeteredBankDecorator<T> {
    fn routing_number(&self) -> String {
        self.bank.routing_number()
    }

    fn reserve_account(&self) -> String {
        self.bank.reserve_account()
    }

    fn accounts(&self) -> Vec<DetailedAccount> {
        self.bank.accounts()
    }

    fn get_enriched_account(&self, account_id: &str) -> Result<DetailedAccount> {
        self.bank.get_enriched_account(account_id)
    }

    fn display_name(&self) -> String {
        self.bank.display_name()
    }

    fn memo_char_limit(&self) -> u32 {
        self.bank.memo_char_limit()
    }

    async fn balance(&self, account_id: &str) -> Result<BankBalance> {
        let res = self.bank.balance(account_id).await;
        meter_bank_method(&self.display_name(), "balance", res)
    }

    async fn transfer(
        &self,
        request: TransferRequest,
        metadata: Option<String>,
    ) -> Result<BankTransferResponse> {
        let res = self.bank.transfer(request, metadata).await;
        meter_bank_method(&self.display_name(), "transfer", res)
    }

    async fn history(
        &self,
        account_id: &str,
        date_range: DateRange,
    ) -> Result<Vec<BankTransaction>> {
        let res = self.bank.history(account_id, date_range).await;
        meter_bank_method(&self.display_name(), "history", res)
    }
}

#[derive(MetricsBag)]
pub struct BankMetricsBag {
    pub request_count: DimensionedCtrDef,
}

impl Default for BankMetricsBag {
    fn default() -> Self {
        Self {
            request_count: DimensionedCtrDef::new(
                vec!["bank", "method", "status"],
                MetricOpts::new(
                "bank_request_counter".to_string(),
                "Track interactions with bank, including transfer request, balance query, and history query".to_string(),
                )
            )
        }
    }
}

pub fn meter_bank_method<T: Any>(bank: &str, method: &str, result: Result<T>) -> Result<T> {
    let status = match &result {
        Ok(_) => "success",
        Err(_) => "fail",
    };
    BANK_METRICS
        .request_count
        .with_dims(vec![bank, method, status])
        .unwrap()
        .inc();
    result
}

#[cfg(test)]
mod tests {
    use super::{MeteredBankDecorator, BANK_METRICS};
    use crate::banks::XandBank;
    use crate::models::account::{Account, TransferRequest};
    use crate::{
        date_range::DateRange, errors::XandBanksErrors, mocks::mock_bank_adapter::MockBankAdapter,
        Bank,
    };
    use futures::executor::block_on;
    use xand_money::{Money, Usd};

    #[test]
    fn balance_success_counter() {
        let mut bank = XandBank {
            name: "".to_string(),
            adapter: MockBankAdapter::default().boxed(),
            routing_number: "".to_string(),
            trust_account: Default::default(),
            accounts: Default::default(),
        };
        bank.accounts.insert("boop".to_string(), Account::default());
        let bank = MeteredBankDecorator::new(bank);
        let dims = vec!["", "balance", "success"];
        let cnt_0 = BANK_METRICS
            .request_count
            .with_dims(dims.clone())
            .unwrap()
            .get();
        let _ = block_on(bank.balance("boop"));
        let cnt_1 = BANK_METRICS.request_count.with_dims(dims).unwrap().get();
        assert_eq!(cnt_0 + 1, cnt_1);
    }

    #[test]
    fn transfer_success_counter() {
        let mut bank = XandBank {
            name: "".to_string(),
            adapter: MockBankAdapter::default().boxed(),
            routing_number: "".to_string(),
            trust_account: Default::default(),
            accounts: Default::default(),
        };
        bank.accounts.insert("boop".to_string(), Account::default());
        bank.accounts.insert("beep".to_string(), Account::default());
        let bank = MeteredBankDecorator::new(bank);
        let dims = vec!["", "transfer", "success"];
        let cnt_0 = BANK_METRICS
            .request_count
            .with_dims(dims.clone())
            .unwrap()
            .get();
        let _ = block_on(bank.transfer(
            TransferRequest::new("beep", "boop", Usd::from_f64_major_units(500.).unwrap()),
            None,
        ));
        let cnt_1 = BANK_METRICS.request_count.with_dims(dims).unwrap().get();
        assert_eq!(cnt_0 + 1, cnt_1);
    }

    #[test]
    fn history_success_counter() {
        let mut bank = XandBank {
            name: "".to_string(),
            adapter: MockBankAdapter::default().boxed(),
            routing_number: "".to_string(),
            trust_account: Default::default(),
            accounts: Default::default(),
        };
        bank.accounts.insert("bop".to_string(), Account::default());
        let bank = MeteredBankDecorator::new(bank);
        let dims = vec!["", "history", "success"];
        let cnt_0 = BANK_METRICS
            .request_count
            .with_dims(dims.clone())
            .unwrap()
            .get();
        let _ = block_on(bank.history("bop", DateRange::default()));
        let cnt_1 = BANK_METRICS.request_count.with_dims(dims).unwrap().get();
        assert_eq!(cnt_0 + 1, cnt_1);
    }
    #[test]
    fn balance_fail_counter() {
        let mut bad_bank = XandBank {
            name: "".to_string(),
            adapter: MockBankAdapter::new(
                Err(XandBanksErrors::Mock),
                Err(XandBanksErrors::Mock),
                Err(XandBanksErrors::Mock),
            )
            .boxed(),
            routing_number: "".to_string(),
            trust_account: Default::default(),
            accounts: Default::default(),
        };
        bad_bank
            .accounts
            .insert("boop".to_string(), Account::default());
        let bad_bank = MeteredBankDecorator::new(bad_bank);
        let dims = vec!["", "balance", "fail"];
        let cnt_0 = BANK_METRICS
            .request_count
            .with_dims(dims.clone())
            .unwrap()
            .get();
        let _ = block_on(bad_bank.balance("boop"));
        let cnt_1 = BANK_METRICS.request_count.with_dims(dims).unwrap().get();
        assert_eq!(cnt_0 + 1, cnt_1);
    }

    #[test]
    fn transfer_fail_counter() {
        let mut bad_bank = XandBank {
            name: "".to_string(),
            adapter: MockBankAdapter::new(
                Err(XandBanksErrors::Mock),
                Err(XandBanksErrors::Mock),
                Err(XandBanksErrors::Mock),
            )
            .boxed(),
            routing_number: "".to_string(),
            trust_account: Default::default(),
            accounts: Default::default(),
        };
        bad_bank
            .accounts
            .insert("beep".to_string(), Account::default());
        bad_bank
            .accounts
            .insert("boop".to_string(), Account::default());

        let bad_bank = MeteredBankDecorator::new(bad_bank);
        let dims = vec!["", "transfer", "fail"];
        let cnt_0 = BANK_METRICS
            .request_count
            .with_dims(dims.clone())
            .unwrap()
            .get();
        let _ = block_on(bad_bank.transfer(
            TransferRequest::new("beep", "boop", Usd::from_f64_major_units(500.).unwrap()),
            None,
        ));
        let cnt_1 = BANK_METRICS.request_count.with_dims(dims).unwrap().get();
        assert_eq!(cnt_0 + 1, cnt_1);
    }

    #[test]
    fn history_fail_counter() {
        let mut bad_bank = XandBank {
            name: "".to_string(),
            adapter: MockBankAdapter::new(
                Err(XandBanksErrors::Mock),
                Err(XandBanksErrors::Mock),
                Err(XandBanksErrors::Mock),
            )
            .boxed(),
            routing_number: "".to_string(),
            trust_account: Default::default(),
            accounts: Default::default(),
        };
        bad_bank
            .accounts
            .insert("bop".to_string(), Account::default());

        let bad_bank = MeteredBankDecorator::new(bad_bank);
        let dims = vec!["", "history", "fail"];
        let cnt_0 = BANK_METRICS
            .request_count
            .with_dims(dims.clone())
            .unwrap()
            .get();
        let _ = block_on(bad_bank.history("bop", DateRange::default()));
        let cnt_1 = BANK_METRICS.request_count.with_dims(dims).unwrap().get();
        assert_eq!(cnt_0 + 1, cnt_1);
    }
}
