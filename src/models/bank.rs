#[derive(Clone, Debug, PartialEq, Eq, Default)]
pub struct BankAdapter {
    pub adapter_id: i32,
    pub adapter_type: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Default)]
pub struct McbAdapterKeys {
    pub mcb_secret_user_name: String,
    pub mcb_secret_password: String,
    pub mcb_secret_client_app_ident: String,
    pub mcb_secret_organization_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Default)]
pub struct TpAdapterKeys {
    pub tp_secret_api_key_id: String,
    pub tp_secret_api_key_value: String,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum BankAdapterKeys {
    Mcb(McbAdapterKeys),
    Tp(TpAdapterKeys),
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Bank {
    pub id: i32,
    pub name: String,
    pub routing_number: String,
    pub trust_account: String,
    pub url: String,
    pub adapter: BankAdapterKeys,
}
