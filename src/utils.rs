use crate::{banks::config::BankConfigurationErrors, Result};
use url::Url;

pub(crate) fn trim_trailing_slash(mut val: String) -> String {
    if val.ends_with('/') {
        val.pop();
    }
    val
}

/// Checks a url for a trailing slash so that we can append path segments to it.
/// If it doesn't have one, add one so it's usable.
pub fn check_or_add_trailing_slash(url: &mut Url) -> Result<Url> {
    if !url.path().ends_with('/') {
        if url.cannot_be_a_base() {
            return Err(BankConfigurationErrors::GenericConfigError {
                msg: format!("Failed to add trailing slash to provided url: {}", url),
            }
            .into());
        }
        // This unwrap will never panic since it can only panic if the url is not a base,
        // which has been checked in the prior statement.
        url.path_segments_mut().unwrap().push("");
    }

    Ok(url.clone())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn adds_slash_to_url_if_needed() {
        let mut url_w_slash = Url::parse("http://whatever.com/ewwwww/").unwrap();
        check_or_add_trailing_slash(&mut url_w_slash).unwrap();
        assert_eq!(
            url_w_slash.join("evenmore").unwrap().as_str(),
            "http://whatever.com/ewwwww/evenmore"
        );
        let mut url_wo_slash = Url::parse("http://blahblahblah.com/heyyyyy/").unwrap();
        check_or_add_trailing_slash(&mut url_wo_slash).unwrap();
        assert_eq!(
            url_wo_slash.join("reymundo").unwrap().as_str(),
            "http://blahblahblah.com/heyyyyy/reymundo"
        )
    }
}
