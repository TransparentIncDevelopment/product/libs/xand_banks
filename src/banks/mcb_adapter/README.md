# MCB Adapter

Overall structure:

```mermaid
graph TD;
  McbAdapter-->DataClientWrapper;
  McbAdapter-->AccessTokenManager;
  AccessTokenManager-->AuthClientWrapper;
  AuthClientWrapper-->mcb_auth_gen::generate_access_token_using_post;
  DataClientWrapper-->mcb_acct_gen::account_inquiry_using_post;
  DataClientWrapper-->mcb_transfer_gen::xfer_add_using_post;
  DataClientWrapper-->mcb_acct_trn_gen::acct_trn_inq_using_post;
```

## Authentication

MCB authentication works in two stages. Unlike Treasury Prime, the MCB API
authenticates with a token; the token is retrieved via a separate endpoint which
in turn requires HTTP Basic auth.

The token-authenticated APIs are wrapped in the `DataClientWrapper` trait, while
the authentication endpoint is wrapped by `AuthClientWrapper`. The `McbAdapter`
uses the `DataClientWrapper` directly when making data requests, while it defers
to the `AccessTokenManager` to manage refreshing the token when needed.
