use super::client_wrapper::ClientConfiguration;
use crate::banks::mcb_adapter::CloneableMcbAdapter;
use crate::{
    utils::{check_or_add_trailing_slash, trim_trailing_slash},
    Result,
};
use std::sync::Arc;
use std::time::Duration;
use url::Url;
use xand_secrets::SecretKeyValueStore;

/// Required configuration values to start using the MCB adapter
#[derive(Serialize, Deserialize, Eq, PartialEq, Clone, Debug, Ord, PartialOrd, Hash)]
#[serde(rename_all = "kebab-case")]
pub struct McbConfig {
    /// The host url to hit. For example, MCB's test env host url is https://dev.mcb-api.com
    #[serde(with = "url_serde")]
    pub url: Url,
    pub refresh_if_expiring_in_secs: Option<u64>,
    pub auth: McbClientTokenRetrievalAuthConfig,
    pub account_info_manager: RawAccountInfoManagerConfig,
    pub timeout: u64,
}

#[derive(Serialize, Deserialize, Eq, PartialEq, Clone, Debug, Ord, PartialOrd, Hash)]
#[serde(rename_all = "kebab-case")]
pub struct McbClientTokenRetrievalAuthConfig {
    pub secret_key_username: String,
    pub secret_key_password: String,
}

impl McbConfig {
    pub fn client_wrapper_config(&self) -> ClientConfiguration {
        ClientConfiguration {
            base_path: trim_trailing_slash(self.url.clone().into_string()),
            user_agent: None,
            timeout: Duration::from_secs(self.timeout),
        }
    }

    pub fn auth_config(&self) -> McbClientTokenRetrievalAuthConfig {
        self.auth.clone()
    }

    pub fn raw_account_info_manager_config(&self) -> RawAccountInfoManagerConfig {
        self.account_info_manager.clone()
    }

    pub fn validate(&self) -> Result<()> {
        // Nothing to validate so this is a no op
        Ok(())
    }
}

#[derive(Serialize, Deserialize, Eq, PartialEq, Clone, Debug, Ord, PartialOrd, Hash)]
#[serde(rename_all = "kebab-case")]
pub struct RawAccountInfoManagerConfig {
    pub secret_key_client_app_ident: String,
    pub secret_key_organization_id: String,
}

fn ensure_valid_url(cfg: &mut McbConfig) -> Result<&McbConfig> {
    cfg.url = check_or_add_trailing_slash(&mut cfg.url.clone())?;
    Ok(cfg)
}

impl McbConfig {
    pub fn get_adapter(
        &self,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> Result<CloneableMcbAdapter> {
        CloneableMcbAdapter::new(ensure_valid_url(&mut self.clone())?, secret_store)
    }
}
