use super::client_wrapper::{RealAuthClient, RealDataClient};
use crate::models::account::TransferRequest;
use crate::models::BankBalance;
use crate::BankAdapter;
use crate::{
    banks::mcb_adapter::{
        access_token_manager::{AccessTokenManager, RefreshPolicy},
        account_info_manager::AccountInfoManager,
        config, McbAdapter,
    },
    date_range::DateRange,
    models::{BankTransaction, BankTransferResponse},
    Result as LibResult,
};
use async_trait::async_trait;
use std::sync::Arc;
use xand_secrets::SecretKeyValueStore;

/// This is a cloneable wrapper for the McbAdapter to satisfy trait restrictions on BankDispatcher/Bank
#[derive(Clone)]
pub struct CloneableMcbAdapter {
    inner_adapter: Arc<McbAdapter<RealDataClient, RealAuthClient, AccountInfoManager>>,
}

impl CloneableMcbAdapter {
    pub fn new(
        cfg: &config::McbConfig,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> LibResult<Self> {
        let refresh_policy = RefreshPolicy::PriorToTokenExpiration(std::time::Duration::from_secs(
            cfg.refresh_if_expiring_in_secs.unwrap_or(60 * 5),
        ));
        let auth_client = RealAuthClient::from_config(cfg.client_wrapper_config())?;
        let token_manager = AccessTokenManager::new(
            refresh_policy,
            cfg.auth_config(),
            secret_store.clone(),
            auth_client,
        );

        let data_client = RealDataClient::from_config(cfg.client_wrapper_config())?;
        let account_info_manager =
            AccountInfoManager::new(cfg.raw_account_info_manager_config(), secret_store.clone());
        let inner_adapter = Arc::new(McbAdapter {
            client_wrapper: data_client,
            token_manager,
            account_info_manager,
        });
        Ok(CloneableMcbAdapter { inner_adapter })
    }
}

#[async_trait]
impl BankAdapter for CloneableMcbAdapter {
    async fn balance(&self, acct_no: &str) -> LibResult<BankBalance> {
        self.inner_adapter
            .get_balance(acct_no)
            .await
            .map_err(Into::into)
    }

    async fn transfer(
        &self,
        request: TransferRequest,
        metadata: Option<String>,
    ) -> LibResult<BankTransferResponse> {
        self.inner_adapter
            .transfer(request, metadata)
            .await
            .map_err(Into::into)
    }

    async fn history(
        &self,
        acct_no: &str,
        date_range: DateRange,
    ) -> LibResult<Vec<BankTransaction>> {
        self.inner_adapter
            .history(acct_no, Some(date_range))
            .await
            .map_err(Into::into)
    }

    fn memo_char_limit(&self) -> u32 {
        33
    }
}
