use chrono::{DateTime, NaiveDate, Utc};
use xand_money::Usd;

/// Known bugs with mcb transaction objects:
///     1. No valid timestamp and timezone: https://www.pivotaltracker.com/story/show/168849189
///     2. No spec of known "Status" values: https://www.pivotaltracker.com/story/show/168852875
#[derive(Debug, Clone)]
pub struct McbTransaction {
    pub unique_id: String,
    pub amount: Usd,
    pub metadata: String,
    /// MCB Docs: "Debit Credit Type. Indicates if the transaction history was a debit or credit to the account."
    pub txn_type: McbTransactionType,
    /// MCB Docs: "Transaction date and time for this transaction as recorded on the history record."
    /// There is not actually a valid timestamp on the transaction just yet . See bug 1 above
    pub txn_date: NaiveDate,
    /// MCB Docs: "Provides the current state of the AcctTrn record."
    pub txn_status: McbTransactionStatus,
    /// MCB Docs: "The date that an associated action takes effect"
    /// There is not actually a valid timestamp on the transaction just yet . See bug 1 above
    pub effect_datetime: DateTime<Utc>,
}

/// TODO: Fix when MCB clarifies possible values than come through
/// https://www.pivotaltracker.com/story/show/168852875
#[derive(Debug, Clone)]
pub enum McbTransactionStatus {
    Valid,
    Other(String),
}

#[derive(Debug, Clone)]
pub enum McbTransactionType {
    Credit,
    Debit,
}
