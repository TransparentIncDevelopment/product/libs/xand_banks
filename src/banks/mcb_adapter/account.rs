use crate::banks::mcb_adapter::errors::Result;
use async_trait::async_trait;
use xand_secrets::Secret;

#[derive(Debug, Clone)]
pub struct AccountInfo {
    pub client_app_ident: Secret<String>,
    pub organization_id: Secret<String>,
    pub account_id: String,
}

/// A trait for retrieving account information for a given account_id
#[async_trait]
pub trait McbAccountInfoManager {
    async fn get_account_info(&self, account_id: &str) -> Result<AccountInfo>;
}
