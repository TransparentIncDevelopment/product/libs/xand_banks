use async_trait::async_trait;

use crate::banks::mcb_adapter::{
    access_token_manager::ClientTokenRetrievalAuthentication, errors::*,
};
use mcb_acct_gen::{
    apis::{
        acct_service_controller_api::{account_inquiry_using_post, AccountInquiryUsingPostError},
        configuration::Configuration as AcctConfiguration,
        Error as AcctError,
    },
    models::{AcctInqRq, AcctInqRs},
};
use mcb_acct_trn_gen::{
    apis::acct_trn_service_controller_api::{acct_trn_inq_using_post, AcctTrnInqUsingPostError},
    apis::configuration::Configuration as TxnConfiguration,
    apis::Error as TxnError,
    models::{AcctTrnInqRq, AcctTrnInqRs},
};
use mcb_auth_gen::{
    apis::{
        access_token_generator_api::{
            generate_access_token_using_post, GenerateAccessTokenUsingPostError,
        },
        configuration::Configuration as AuthConfiguration,
        Error as AuthError,
    },
    models::AccessTokenOutput,
};
use mcb_transfer_gen::{
    apis::{
        configuration::Configuration as TransferConfiguration,
        transfer_service_controller_api::{xfer_add_using_post, XferAddUsingPostError},
        Error as TransferError,
    },
    models::{XferReq, XferRes},
};
use std::sync::Arc;
use std::time::Duration;
use xand_secrets::{ExposeSecret, Secret};

pub struct ClientConfiguration {
    pub base_path: String,
    pub user_agent: Option<String>,
    pub timeout: Duration,
}

/// Exposes only the behavior we need from the underlying API, using our own error types, to better
/// support testing.
#[async_trait]
pub trait AuthClientWrapper: Send + Sync {
    async fn get_auth_token(
        &self,
        auth: ClientTokenRetrievalAuthentication,
    ) -> Result<AccessTokenOutput>;
}

#[async_trait]
pub trait DataClientWrapper: Send + Sync {
    async fn account_balance(
        &self,
        token: &Secret<String>,
        acct_info: AcctInqRq,
    ) -> Result<AcctInqRs>;
    async fn transfer(&self, token: &Secret<String>, req: XferReq) -> Result<XferRes>;
    async fn history(&self, token: &Secret<String>, req: AcctTrnInqRq) -> Result<AcctTrnInqRs>;
}

#[derive(Clone)]
pub struct RealAuthClient {
    config: Arc<ClientConfiguration>,
    client: reqwest::Client,
}

impl RealAuthClient {
    pub fn from_config(config: ClientConfiguration) -> Result<Self> {
        reqwest::Client::builder()
            .timeout(config.timeout)
            .build()
            .map(|client| Self {
                config: Arc::new(config),
                client,
            })
            .map_err(|e| e.into())
    }
}

/// These are the only known valid values for `grant_type` and `scope` for MCB
const GRANT_TYPE: &str = "client_credentials";
const SCOPE: &str = "ScopeTS";

impl RealAuthClient {
    fn merge_configuration(&self, auth: &ClientTokenRetrievalAuthentication) -> AuthConfiguration {
        let ClientTokenRetrievalAuthentication { username, password } = auth;

        AuthConfiguration {
            base_path: self.config.base_path.clone(),
            user_agent: self.config.user_agent.clone(),
            client: self.client.clone(),
            basic_auth: Some((
                username.expose_secret().clone(),
                Some(password.expose_secret().clone()),
            )),
            oauth_access_token: None,
            bearer_access_token: None,
            api_key: None,
        }
    }
}

#[async_trait]
impl AuthClientWrapper for RealAuthClient {
    async fn get_auth_token(
        &self,
        auth: ClientTokenRetrievalAuthentication,
    ) -> Result<AccessTokenOutput> {
        let config = self.merge_configuration(&auth);

        generate_access_token_using_post(&config, Some(GRANT_TYPE), Some(SCOPE))
            .await
            .map_err(Into::into)
    }
}

#[derive(Clone)]
pub struct RealDataClient {
    config: Arc<ClientConfiguration>,
    client: reqwest::Client,
}

impl RealDataClient {
    pub fn from_config(config: ClientConfiguration) -> Result<Self> {
        reqwest::Client::builder()
            .timeout(config.timeout)
            .build()
            .map(|client| Self {
                config: Arc::new(config),
                client,
            })
            .map_err(|e| e.into())
    }
}

#[async_trait]
impl DataClientWrapper for RealDataClient {
    async fn account_balance(
        &self,
        token: &Secret<String>,
        acct_info: AcctInqRq,
    ) -> Result<AcctInqRs> {
        let config = AcctConfiguration {
            base_path: self.config.base_path.clone(),
            user_agent: self.config.user_agent.clone(),
            client: self.client.clone(),
            basic_auth: None,
            oauth_access_token: Some(token.expose_secret().clone()),
            bearer_access_token: None,
            api_key: None,
        };

        account_inquiry_using_post(&config, acct_info)
            .await
            .map_err(Into::into)
    }

    async fn transfer(&self, token: &Secret<String>, req: XferReq) -> Result<XferRes> {
        let config = TransferConfiguration {
            base_path: self.config.base_path.clone(),
            user_agent: self.config.user_agent.clone(),
            client: self.client.clone(),
            basic_auth: None,
            oauth_access_token: Some(token.expose_secret().clone()),
            bearer_access_token: None,
            api_key: None,
        };

        xfer_add_using_post(&config, req).await.map_err(Into::into)
    }

    async fn history(&self, token: &Secret<String>, req: AcctTrnInqRq) -> Result<AcctTrnInqRs> {
        let config = TxnConfiguration {
            base_path: self.config.base_path.clone(),
            user_agent: self.config.user_agent.clone(),
            client: self.client.clone(),
            basic_auth: None,
            oauth_access_token: Some(token.expose_secret().clone()),
            bearer_access_token: None,
            api_key: None,
        };

        acct_trn_inq_using_post(&config, req)
            .await
            .map_err(Into::into)
    }
}

// Each auto-generated API method produces an error enum with exactly the same structure.
macro_rules! impl_from_generated_error_for_mcb_http_error {
    ($generated_error_type:ident < $response_type:ident >) => {
        impl From<$generated_error_type<$response_type>> for McbHttpError {
            fn from(e: $generated_error_type<$response_type>) -> McbHttpError {
                match e {
                    $generated_error_type::Reqwest(e) => McbHttpError::Reqwest {
                        source: Arc::new(e),
                    },
                    $generated_error_type::Serde(e) => McbHttpError::Serde {
                        source: Arc::new(e),
                    },
                    $generated_error_type::Io(e) => McbHttpError::Io {
                        source: Arc::new(e),
                    },
                    $generated_error_type::ResponseError(e) => McbHttpError::Response {
                        status_code: e.status,
                        content: e.content,
                    },
                }
            }
        }
    };
}

impl_from_generated_error_for_mcb_http_error!(AuthError<GenerateAccessTokenUsingPostError>);
impl_from_generated_error_for_mcb_http_error!(AcctError<AccountInquiryUsingPostError>);
impl_from_generated_error_for_mcb_http_error!(TransferError<XferAddUsingPostError>);
impl_from_generated_error_for_mcb_http_error!(TxnError<AcctTrnInqUsingPostError>);
