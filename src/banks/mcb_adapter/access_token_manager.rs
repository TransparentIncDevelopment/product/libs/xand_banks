use mcb_auth_gen::models::AccessTokenOutput;
use std::convert::TryFrom;
use std::convert::TryInto;
use std::time::Duration;
use std::{sync::Arc, time::Instant};

use crate::{
    banks::mcb_adapter::errors::*,
    banks::mcb_adapter::{
        client_wrapper::AuthClientWrapper, config::McbClientTokenRetrievalAuthConfig,
        errors::McbHttpError::IncompleteMcbHttpResponseBody,
    },
};
use futures::lock::Mutex;
use snafu::ResultExt;
use xand_secrets::{Secret, SecretKeyValueStore};

/// Authentication tokens expiration is set by the bank. For example, a bank may issue a token
/// that expires in 86400 seconds (24 hours). RefreshPolicy::IfExpiringInTheNext specifies
/// the buffer time before expiration to proactively refresh the token.
#[derive(Clone)]
pub enum RefreshPolicy {
    /// Buffer time before expiration to proactively refresh the token.
    PriorToTokenExpiration(Duration),
}

#[derive(Clone, Debug)]
pub struct ClientTokenRetrievalAuthentication {
    pub username: Secret<String>,
    pub password: Secret<String>,
}

pub struct AccessTokenManager<C: AuthClientWrapper> {
    refresh_policy: RefreshPolicy,
    client: C,
    secret_store: Arc<dyn SecretKeyValueStore>,
    auth_config: McbClientTokenRetrievalAuthConfig,
    token_info: Mutex<Option<AccessTokenInfo>>,
}

#[derive(Debug)]
pub(crate) enum LoggingEvents {
    ReachingOutForNewToken,
}

#[derive(Clone)]
struct AccessTokenInfo {
    token: Secret<String>,
    fetched_at: Instant,
    expires_in: Duration,
}

impl AccessTokenInfo {
    pub fn should_refresh(&self, policy: &RefreshPolicy) -> bool {
        match policy {
            RefreshPolicy::PriorToTokenExpiration(duration) => {
                // Refresh if we are within or past the policy's threshold
                let now = Instant::now();
                let expires_at = self.fetched_at + self.expires_in;
                let refresh_at = expires_at - *duration;
                now > refresh_at
            }
        }
    }
}

impl TryFrom<AccessTokenOutput> for AccessTokenInfo {
    type Error = McbHttpError;

    fn try_from(auth_token_resp: AccessTokenOutput) -> Result<Self> {
        let expires_in_secs =
            auth_token_resp
                .expires_in
                .ok_or_else(|| IncompleteMcbHttpResponseBody {
                    message: "AccessTokenOutput.expires_in is None".to_string(),
                })?;
        let expires_in_secs: u64 = expires_in_secs.try_into().context(CastError {
            message: format!(
                "Unable to parse expires_in_secs from signed to unsigned for value: {:?}",
                expires_in_secs
            ),
        })?;

        Ok(Self {
            fetched_at: Instant::now(),
            expires_in: Duration::from_secs(expires_in_secs),
            token: Secret::new(auth_token_resp.access_token.unwrap()),
        })
    }
}

impl<C: AuthClientWrapper> AccessTokenManager<C> {
    pub fn new(
        refresh_policy: RefreshPolicy,
        auth_config: McbClientTokenRetrievalAuthConfig,
        secret_store: Arc<dyn SecretKeyValueStore>,
        client: C,
    ) -> Self {
        Self {
            refresh_policy,
            client,
            secret_store,
            auth_config,
            token_info: Mutex::new(None),
        }
    }

    /// Locks to perform the operations to get or update the token
    pub async fn get_or_refresh_locked(&self) -> Result<Secret<String>> {
        let mut cur_token_info = self.token_info.lock().await;
        let next_token_info = self.get_or_refresh(&cur_token_info).await?;
        let cur_token = next_token_info.token.clone();
        *cur_token_info = Some(next_token_info);
        Ok(cur_token)
    }

    async fn retrieve_credentials_for_bank(&self) -> Result<ClientTokenRetrievalAuthentication> {
        let McbClientTokenRetrievalAuthConfig {
            secret_key_username,
            secret_key_password,
        } = &self.auth_config;
        let username = self.secret_store.read(secret_key_username.as_str()).await?;
        let password = self.secret_store.read(secret_key_password.as_str()).await?;
        Ok(ClientTokenRetrievalAuthentication { username, password })
    }

    /// Fetches new token if `token_info` is None or RefreshPolicy evaluates to true, else returns a clone of the token
    /// to use in requests
    async fn get_or_refresh(
        &self,
        maybe_token_info: &Option<AccessTokenInfo>,
    ) -> Result<AccessTokenInfo> {
        let possible_token_result = maybe_token_info
            .as_ref()
            .and_then(
                // There exists a token value. If `should_refresh()` returns true, pass None along
                |token_info| {
                    if token_info.should_refresh(&self.refresh_policy) {
                        None
                    } else {
                        Some(token_info)
                    }
                },
            )
            .map(
                // Existing token is still valid
                |valid_token_info| Ok(valid_token_info.clone()),
            );

        if let Some(value) = possible_token_result {
            value
        } else {
            tracing::debug!(message = ?LoggingEvents::ReachingOutForNewToken);
            let root_auth = self.retrieve_credentials_for_bank().await?;
            // Token is None or needs to be refreshed
            self.client.get_auth_token(root_auth).await?.try_into()
        }
    }
}
