use crate::banks::mcb_adapter::errors::{McbHttpError::IncompleteMcbHttpResponseBody, *};

use crate::banks::mcb_adapter::request_models::Transfer;
use mcb_transfer_gen::models::{
    AcctKeys, AcctType, Client, CurAmt, EfxHdr, FromAcctRef, Organization, ToAcctRef, Tracking,
    XferInfo, XferReq, XferReqXferReq, XferRes,
};
use std::convert::TryFrom;
use xand_secrets::ExposeSecret;

/// Extension methods for the Request-side object
pub trait XferRqExt {
    fn fill_with(xfer: Transfer) -> Result<Box<Self>>;
}

impl XferRqExt for XferReq {
    fn fill_with(xfer: Transfer) -> Result<Box<Self>> {
        let src_acct = xfer.src_acct;
        let account_id = src_acct.account_id.clone();
        let org_id = src_acct.organization_id;
        let client_app_ident = src_acct.client_app_ident;
        let metadata = xfer.metadata.clone();
        let dest_acct_id = xfer.dest_acct_id.clone();
        let amt = xfer.amount;
        let tracking_id = uuid::Uuid::new_v4().to_string();

        Ok(Box::new(XferReq {
            xfer_req: Some(Box::new(XferReqXferReq {
                efx_hdr: Some(Box::new(EfxHdr {
                    client: Some(Box::new(Client {
                        organization: Some(Box::new(Organization {
                            org_id: Some(org_id.expose_secret().clone()),
                        })),
                        client_app_ident: Some(client_app_ident.expose_secret().clone()),
                    })),
                    /// This optional tracking info is used for logging and traceability purposes
                    tracking: Some(Box::new(Tracking {
                        /// Uuid for first message in a sequence
                        originating_trn_id: Some(tracking_id.clone()),
                        /// Uuid for prior message in sequence
                        parent_trn_id: Some(tracking_id.clone()),
                        /// Uuid for current message
                        trn_id: Some(tracking_id),
                    })),
                    version: Some("1.2".to_string()),
                })),
                xfer_info: Some(Box::new(XferInfo {
                    xfer_to_desc: metadata.clone(),
                    xfer_from_desc: metadata,
                    from_acct_ref: Some(Box::new(FromAcctRef {
                        acct_keys: Some(Box::new(AcctKeys {
                            acct_id: Some(account_id),
                            acct_type: Some(Box::new(AcctType {
                                acct_type_source: Some("IFX".to_string()),
                                acct_type_value: Some("DDA".to_string()),
                            })),
                        })),
                        trn_code: None,
                    })),
                    to_acct_ref: Some(Box::new(ToAcctRef {
                        acct_keys: Some(Box::new(AcctKeys {
                            acct_id: Some(dest_acct_id),
                            acct_type: Some(Box::new(AcctType {
                                acct_type_source: Some("IFX".to_string()),
                                acct_type_value: Some("DDA".to_string()),
                            })),
                        })),
                        trn_code: None,
                    })),
                    cur_amt: Some(Box::new(CurAmt {
                        // XferReqXferReq type does not allow failure case for field
                        amt: Some(f64::try_from(amt).unwrap_or_else(|e| panic!("{}", e))),
                        cur_code: None,
                    })),
                    category: Some("Regular".to_string()),
                })),
            })),
        }))
    }
}
/// Extension trait for the Response-side object
pub trait XferResExt {
    fn get_transfer_response(self) -> Result<String>;
}

impl XferResExt for XferRes {
    fn get_transfer_response(self) -> Result<String> {
        self.xfer_res
            .and_then(|r| r.xfer_status)
            .and_then(|r| r.xfer_status_rec)
            .and_then(|r| r.xfer_status)
            .and_then(|r| r.xfer_status_code)
            .ok_or_else(|| IncompleteMcbHttpResponseBody {
                message: "Transfer response does not contain status".to_string(),
            })
    }
}
