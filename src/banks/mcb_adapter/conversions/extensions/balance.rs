use crate::banks::mcb_adapter::{
    account::AccountInfo,
    conversions::BalTypeSource,
    errors::{McbHttpError::IncompleteMcbHttpResponseBody, *},
};
use mcb_acct_gen::models::{
    AcctBal, AcctInqRq, AcctInqRqAcctInqRq, AcctInqRs, AcctSel, AcctType, Client, EfxHdr,
    Organization, Tracking,
};

use std::str::FromStr;
use uuid::Uuid;
use xand_secrets::ExposeSecret;

/// Extension trait for the Request-side object
pub trait AcctInqRqExt {
    fn fill_with(acct_info: AccountInfo) -> Result<Box<AcctInqRq>>;
}

impl FromStr for BalTypeSource {
    type Err = McbHttpError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "avail" => Ok(BalTypeSource::Avail),
            "current" => Ok(BalTypeSource::Current),
            _ => Err(McbHttpError::BalTypeError {
                input: s.to_string(),
            }),
        }
    }
}

impl AcctInqRqExt for AcctInqRq {
    /// Util method to build the MCB Account Balance request body given
    fn fill_with(acct_info: AccountInfo) -> Result<Box<AcctInqRq>> {
        let account_id = acct_info.account_id.clone();
        let org_id = acct_info.organization_id;
        let client_app_ident = acct_info.client_app_ident;
        let tracking_id = Uuid::new_v4().to_string();

        Ok(Box::new(AcctInqRq {
            acct_inq_rq: Some(Box::new(AcctInqRqAcctInqRq {
                acct_sel: Some(Box::new(AcctSel {
                    acct_id: Some(account_id),
                    acct_type: Some(Box::new(AcctType {
                        acct_type_source: Some("IFX".to_string()),
                        acct_type_value: Some("DDA".to_string()),
                    })),
                })),
                efx_hdr: Some(Box::new(EfxHdr {
                    client: Some(Box::new(Client {
                        organization: Some(Box::new(Organization {
                            org_id: Some(org_id.expose_secret().clone()),
                        })),
                        client_app_ident: Some(client_app_ident.expose_secret().clone()),
                    })),
                    /// This optional tracking info is used for logging and traceability purposes
                    tracking: Some(Box::new(Tracking {
                        /// Uuid for first message in a sequence
                        originating_trn_id: Some(tracking_id.clone()),
                        /// Uuid for prior message in sequence
                        parent_trn_id: Some(tracking_id.clone()),
                        /// Uuid for current message
                        trn_id: Some(tracking_id),
                    })),
                    version: Some("1.2".to_string()),
                })),
            })),
        }))
    }
}
/// Extension trait for the Response-side object
pub trait AcctInqRsExt {
    /// From the MCB Balance response, get the list of Balances, Err if not present
    fn get_balance_vec(self) -> Result<Vec<AcctBal>>;
}

impl AcctInqRsExt for AcctInqRs {
    /// From the MCB Balance response, get the list of Balances, Err if not present
    fn get_balance_vec(self) -> Result<Vec<AcctBal>> {
        // Go down the option chain until we get to the account balance vec
        self.acct_inq_rs
            .and_then(|inner_acct_rs| inner_acct_rs.acct_rec)
            .and_then(|acct_rec| acct_rec.acct_info)
            .and_then(|acct_info| acct_info.acct_bal)
            .ok_or_else(|| IncompleteMcbHttpResponseBody {
                message: "Account Balance response does not contain vec of AcctBal".to_string(),
            })
    }
}

pub trait AcctBalExt {
    /// False if path to bal_type_src has None, or true/false if lowercase, trimmed `bal_type_src` is equal to val
    fn bal_type_src_eq(&self, val: &BalTypeSource) -> bool;
}

impl AcctBalExt for AcctBal {
    fn bal_type_src_eq(&self, val: &BalTypeSource) -> bool {
        self.bal_type
            .as_ref()
            .and_then(|bal_type| {
                bal_type
                    .bal_type_source
                    .as_ref()?
                    .to_lowercase()
                    .trim()
                    .parse::<BalTypeSource>()
                    .ok()
            })
            .map_or(false, |v| v == *val)
    }
}
