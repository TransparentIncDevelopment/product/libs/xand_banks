use crate::banks::mcb_adapter::errors::{McbHttpError::IncompleteMcbHttpResponseBody, *};

use crate::banks::mcb_adapter::transaction::{
    McbTransaction, McbTransactionStatus, McbTransactionType,
};
use chrono::{DateTime, NaiveDate, NaiveDateTime, Utc};

use crate::banks::mcb_adapter::conversions::extensions::history::util::format_mcb_datetime_string;
use crate::banks::mcb_adapter::errors::McbHttpError;
use crate::banks::mcb_adapter::request_models::{HistoryParams, McbCursor};
use mcb_acct_trn_gen::models::{
    AcctKeys, AcctTrnInqRq, AcctTrnInqRqAcctTrnInqRq, AcctTrnInqRs, AcctTrnRec, AcctTrnSel,
    AcctType, Client, DtRange, EfxHdr, Organization, RecCtrlIn, Tracking,
};
use snafu::ResultExt;
use std::convert::TryInto;
use xand_money::{Money, Usd};
use xand_secrets::ExposeSecret;

/// Extension methods for the Request-side object
pub trait AcctTrnInqRqExt {
    fn fill_with(history_params: &HistoryParams) -> Result<Box<Self>>;
}

impl AcctTrnInqRqExt for AcctTrnInqRq {
    fn fill_with(history_params: &HistoryParams) -> Result<Box<Self>> {
        // Set up account info
        let account_id = history_params.acct.account_id.clone();
        let org_id = &history_params.acct.organization_id;
        let client_app_ident = &history_params.acct.client_app_ident;
        let tracking_id = uuid::Uuid::new_v4().to_string();

        // Setup date range params if Some
        let (begin_date, end_date) =
            history_params
                .date_range
                .as_ref()
                .map_or((None, None), |dr| {
                    let begin_formatted = format_mcb_datetime_string(&dr.begin_date());
                    let end_formatted = format_mcb_datetime_string(&dr.end_date());
                    (Some(begin_formatted), Some(end_formatted))
                });

        // If present, set up pagination params
        let raw_pgn_params = history_params.pagination_params.as_ref().map(|params| {
            Box::new(RecCtrlIn {
                cursor: Some(params.cursor.0.clone()),
            })
        });

        Ok(Box::new(AcctTrnInqRq {
            acct_trn_inq_rq: Some(Box::new(AcctTrnInqRqAcctTrnInqRq {
                acct_trn_sel: Some(Box::new(AcctTrnSel {
                    acct_keys: Some(Box::new(AcctKeys {
                        acct_id: Some(account_id),
                        acct_type: Some(Box::new(AcctType {
                            acct_type_source: Some("IFX".to_string()),
                            acct_type_value: Some("DDA".to_string()),
                        })),
                    })),
                    branch_ident: None,
                    chk_num_range: None,
                    cur_amt_range: None,
                    dt_range: Some(vec![DtRange {
                        to_date: end_date,
                        from_date: begin_date,
                    }]),
                    period_type: None,
                    sort_order: None,
                    teller_idet: None,
                    trn_src: None,
                    trn_type: None,
                })),
                efx_hdr: Some(Box::new(EfxHdr {
                    client: Some(Box::new(Client {
                        organization: Some(Box::new(Organization {
                            org_id: Some(org_id.expose_secret().clone()),
                        })),
                        client_app_ident: Some(client_app_ident.expose_secret().clone()),
                    })),
                    /// This optional tracking info is used for logging and traceability purposes
                    tracking: Some(Box::new(Tracking {
                        /// Uuid for first message in a sequence
                        originating_trn_id: Some(tracking_id.clone()),
                        /// Uuid for prior message in sequence
                        parent_trn_id: Some(tracking_id.clone()),
                        /// Uuid for current message
                        trn_id: Some(tracking_id),
                    })),
                    version: Some("1.2".to_string()),
                })),
                rec_ctrl_in: raw_pgn_params,
            })),
        }))
    }
}

mod util {
    use chrono::{DateTime, SecondsFormat, Utc};

    /// MCB's datetime format is the rfc3339 format but without any trailing "Z" or the timezone offset.
    /// This functions formats the datetime string to their documentation.
    /// Ex. "2015-12-31T18:33:17.395376Z" --> "2015-12-31T18:33:17.395376"
    pub fn format_mcb_datetime_string(datetime: &DateTime<Utc>) -> String {
        let mut datetime_str = datetime.to_rfc3339_opts(SecondsFormat::Micros, true);

        // Remove the trailing "Z"
        datetime_str.pop();
        datetime_str
    }

    #[test]
    fn test_format_mcb_datetime_string() {
        let datetime: DateTime<Utc> = DateTime::parse_from_rfc3339("2015-12-31T18:33:17.395376Z")
            .unwrap()
            .into();
        let formatted = format_mcb_datetime_string(&datetime);
        assert_eq!(formatted, "2015-12-31T18:33:17.395376");
    }
}

/// Extension trait for the Response-side object
pub trait AcctTrnInqRsExt {
    fn get_page_and_cursor(&self) -> Result<(Vec<McbTransaction>, Option<McbCursor>)>;
    fn get_bank_transactions(&self) -> Result<Vec<McbTransaction>>;
    fn get_raw_txn_vec(&self) -> Result<&Vec<AcctTrnRec>>;
    fn get_cursor(&self) -> Result<Option<McbCursor>>;
}

impl AcctTrnInqRsExt for AcctTrnInqRs {
    fn get_page_and_cursor(&self) -> Result<(Vec<McbTransaction>, Option<McbCursor>)> {
        let bank_txns = self.get_bank_transactions()?;
        let cursor = self.get_cursor()?;
        Ok((bank_txns, cursor))
    }

    fn get_bank_transactions(&self) -> Result<Vec<McbTransaction>> {
        let mut txns = vec![];
        for txn in (self.get_raw_txn_vec())? {
            let bank_txn: McbTransaction = (txn).try_into()?;
            txns.push(bank_txn)
        }

        Ok(txns)
    }

    fn get_raw_txn_vec(&self) -> Result<&Vec<AcctTrnRec>> {
        self.acct_trn_inq_rs
            .as_ref()
            .and_then(|r| r.acct_trn_rec.as_ref())
            .ok_or_else(|| IncompleteMcbHttpResponseBody {
                message:
                    "Account transaction API response does not contain account transaction records"
                        .to_string(),
            })
    }

    fn get_cursor(&self) -> Result<Option<McbCursor>> {
        let pagination_info = self
            .acct_trn_inq_rs
            .as_ref()
            .and_then(|inner_acct_rs| inner_acct_rs.rec_ctrl_out.as_ref())
            .ok_or_else(|| IncompleteMcbHttpResponseBody {
                message: "History response does not contain pagination info".to_string(),
            })?;
        let maybe_cursor = pagination_info
            .cursor
            .as_ref()
            .map(|cursor_val| McbCursor(cursor_val.clone()));
        Ok(maybe_cursor)
    }
}

pub trait AcctTrnRecExt {
    fn get_txn_id(&self) -> Result<String>;
    fn get_txn_amount(&self) -> Result<Usd>;
    fn get_txn_metadata(&self) -> Result<String>;
    fn get_txn_type(&self) -> Result<McbTransactionType>;
    fn get_txn_date(&self) -> Result<NaiveDate>;
    fn get_txn_status_and_effective_datetime(
        &self,
    ) -> Result<(McbTransactionStatus, DateTime<Utc>)>;
}

impl AcctTrnRecExt for AcctTrnRec {
    fn get_txn_id(&self) -> Result<String> {
        self.acct_trn_id
            .as_ref()
            .map(Clone::clone)
            .ok_or_else(|| IncompleteMcbHttpResponseBody {
                message: "Transaction record does not contain id".to_string(),
            })
    }

    fn get_txn_amount(&self) -> Result<Usd> {
        let amt = self
            .acct_trn_info
            .as_ref()
            .and_then(|r| r.trn_amt.as_ref())
            .and_then(|r| r.amt.as_ref())
            .ok_or_else(|| IncompleteMcbHttpResponseBody {
                message: "Transaction record does not contain amount".to_string(),
            })?;
        Usd::from_str(amt).map_err(|e| McbHttpError::MoneyError { source: e })
    }

    fn get_txn_metadata(&self) -> Result<String> {
        self.acct_trn_info
            .as_ref()
            .and_then(|r| r.desc.clone())
            .ok_or_else(|| IncompleteMcbHttpResponseBody {
                message: "Transaction record does not contain metadata".to_string(),
            })
    }

    fn get_txn_type(&self) -> Result<McbTransactionType> {
        let txn_type_str = self
            .acct_trn_info
            .as_ref()
            .and_then(|r| r.dr_cr_type.clone())
            .ok_or_else(|| IncompleteMcbHttpResponseBody {
                message: "Transaction record does not contain a transaction type".to_string(),
            })?;
        let txn_type = match txn_type_str.to_lowercase().trim() {
            "credit" => McbTransactionType::Credit,
            "debit" => McbTransactionType::Debit,
            _ => {
                // Unexpected value found
                return Err(McbHttpError::ParseTransactionTypeError {
                    value: txn_type_str,
                });
            }
        };
        Ok(txn_type)
    }

    /// Extract txn date from MCB transaction object.
    /// The string value has a "timestamp" but it is all 0s, and timezone isn't known
    /// TODO: Fix here once followed up with MCB: https://www.pivotaltracker.com/story/show/168849189
    fn get_txn_date(&self) -> Result<NaiveDate> {
        let txn_date_str = self
            .acct_trn_info
            .as_ref()
            .and_then(|r| r.trn_dt.as_ref())
            .ok_or_else(|| IncompleteMcbHttpResponseBody {
                message: "Transaction record does not contain a transaction type".to_string(),
            })?;

        let datetime = parse_mcb_datetime_str(txn_date_str)?;
        let date = datetime.date_naive();
        Ok(date)
    }

    /// Extract txn status and "EffectiveDate" from MCB transaction object.
    /// According to MCB Docs: EffectiveDate is "The date that an associated action takes effect".
    /// There are two fields described in their docs as "EffectiveDate", one always has an empty
    /// string, and this one looks incorrect.
    /// Notes on incorrectness of "EffectiveDate":
    /// The string value is a datetime with no timezone. It also looks invalid, as if their
    /// test environment is generating them on the fly (and not the actual timestamp the transaction went
    /// into effect). Timezone *looks* like it is ET from Postman, as the timestamp is always (+2 hours - ~2 minutes) from current PT.
    /// Not going to accomodate that for now, as the timestamp is probably incorrect. So, "assuming" UTC
    /// TODO: Fix here once followed up with MCB: https://www.pivotaltracker.com/story/show/168849189
    fn get_txn_status_and_effective_datetime(
        &self,
    ) -> Result<(McbTransactionStatus, DateTime<Utc>)> {
        let txn_status_obj =
            self.acct_trn_status
                .as_ref()
                .ok_or_else(|| IncompleteMcbHttpResponseBody {
                    message: "Transaction record does not contain a AcctTrnStatus value"
                        .to_string(),
                })?;
        let txn_effect_datetime_str =
            txn_status_obj
                .eff_dt
                .as_ref()
                .ok_or_else(|| IncompleteMcbHttpResponseBody {
                    message: "Transaction record does not contain a EffDt value".to_string(),
                })?;
        let txn_effect_datetime = parse_mcb_datetime_str(txn_effect_datetime_str)?;

        let txn_status_str = txn_status_obj
            .acct_trn_status_code
            .as_ref()
            .ok_or_else(|| IncompleteMcbHttpResponseBody {
                message: "Transaction record does not contain a AcctTrnStatusCode value"
                    .to_string(),
            })?;
        let txn_status = match txn_status_str.to_lowercase().trim() {
            "valid" => McbTransactionStatus::Valid,
            other => McbTransactionStatus::Other(other.to_string()),
        };
        Ok((txn_status, txn_effect_datetime))
    }
}

/// Parse the timezone-less MCB date string returned. Example: "2019-09-30T10:25:02.239"
/// We assume the timezone is UTC.
/// Followup with MCB regarding correct datetimes: https://www.pivotaltracker.com/story/show/168849189
fn parse_mcb_datetime_str(datetime_str: &str) -> Result<DateTime<Utc>> {
    let no_timezone = NaiveDateTime::parse_from_str(datetime_str, "%Y-%m-%dT%H:%M:%S%.3f")
        .context(ParseDatetimeError {
            input: datetime_str.to_string(),
        })?;
    let assumed_utc_dt = DateTime::from_utc(no_timezone, chrono::Utc);
    Ok(assumed_utc_dt)
}

#[test]
fn test_parse_mcb_datetime_str() {
    let datetime_str = "2019-09-30T10:25:02.239";
    let result = parse_mcb_datetime_str(datetime_str).unwrap();

    let rfc_format = format!("{}Z", "2019-09-30T10:25:02.239");
    let expected = DateTime::parse_from_rfc3339(rfc_format.as_str()).unwrap();
    assert_eq!(result, expected);
}
