use crate::banks::mcb_adapter::errors::{McbHttpError::IncompleteMcbHttpResponseBody, *};

pub(super) mod balance {
    use super::*;
    use crate::banks::mcb_adapter::conversions::extensions::balance::AcctBalExt;
    use crate::banks::mcb_adapter::conversions::BalTypeSource;
    use mcb_acct_gen::models::AcctBal;
    use xand_money::{Money, Usd};

    /// Find and parse balance value for the element where `balance_src_type` = `val`. Errors if not found
    /// or balance string cannot be parsed into a float
    pub fn get_balance_where_src_eq(acct_bal: &[AcctBal], bal_type: &BalTypeSource) -> Result<Usd> {
        let balance_entry: Result<&AcctBal> = acct_bal
            .iter()
            .find(|entry| entry.bal_type_src_eq(bal_type))
            .ok_or_else(|| IncompleteMcbHttpResponseBody {
                message: format!(
                    "AcctBal vec does not contain record for balance type: {:?}",
                    &bal_type
                ),
            });

        // Amount that corresponds to the appropriate balance type (e.g. Available Balance Amt)
        let balance_str = balance_entry?
            .cur_amt
            .as_ref()
            .and_then(|cur_amt| cur_amt.amt.as_ref())
            .ok_or_else(|| IncompleteMcbHttpResponseBody {
                message: "CurAmt vec does not contain expected current amount field".to_string(),
            })?;

        Usd::from_str(balance_str).map_err(|e| McbHttpError::MoneyError { source: e })
    }
}
