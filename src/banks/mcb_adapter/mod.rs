use crate::banks::mcb_adapter::account::AccountInfo;
use crate::models::account::TransferRequest;
use crate::{
    banks::mcb_adapter::{
        account::McbAccountInfoManager,
        conversions::extensions::history::AcctTrnInqRsExt,
        errors::*,
        request_models::{HistoryParams, Transfer},
        request_models::{McbCursor, PaginationParams},
        transaction::McbTransaction,
    },
    date_range::DateRange,
    models::{BankBalance, BankTransaction, BankTransferResponse},
};
use access_token_manager::AccessTokenManager;
use client_wrapper::{AuthClientWrapper, DataClientWrapper};
pub use cloneable_mcb_adapter::CloneableMcbAdapter;
use mcb_transfer_gen::models::XferReq;
use std::convert::TryInto;
use xand_secrets::Secret;

mod access_token_manager;
mod account;
mod account_info_manager;
pub mod client_wrapper;
mod cloneable_mcb_adapter;
pub mod config;
mod conversions;
pub mod errors;
mod request_models;
#[cfg(test)]
mod test;
mod transaction;

/// This wraps the raw HTTP client and does token management to offer a simpler interface to Xand components
pub struct McbAdapter<C: DataClientWrapper, A: AuthClientWrapper, I: McbAccountInfoManager> {
    client_wrapper: C,
    token_manager: AccessTokenManager<A>,
    account_info_manager: I,
}

#[derive(Debug)]
enum LoggingEvents {
    GettingToken,
    RetrievedToken,
    GettingAccountInfo,
    AccountInfoRetrieved,
    GettingAccountBalance,
    AccountBalanceRetrieved,
    AttemptTransfer,
    TransferSucceeded,
    GettingHistory,
    HistoryRetrieved,
}

impl<C: DataClientWrapper, A: AuthClientWrapper, I: McbAccountInfoManager> McbAdapter<C, A, I> {
    pub fn new(client_wrapper: C, token_manager: AccessTokenManager<A>, info_mgr: I) -> Self {
        Self {
            client_wrapper,
            token_manager,
            account_info_manager: info_mgr,
        }
    }

    #[tracing::instrument(skip(self))]
    pub async fn get_balance(&self, account_no: &str) -> Result<BankBalance> {
        let token = self.get_token().await?;
        let account_info = self.get_account_info(account_no).await?;

        tracing::debug!(message = ?LoggingEvents::GettingAccountBalance);
        let raw_resp = self
            .client_wrapper
            .account_balance(&token, account_info.try_into()?)
            .await?;
        tracing::debug!(message = ?LoggingEvents::AccountBalanceRetrieved);
        raw_resp.try_into()
    }

    #[tracing::instrument(skip(self))]
    pub async fn transfer(
        &self,
        request: TransferRequest,
        metadata: Option<String>,
    ) -> Result<BankTransferResponse> {
        let token = self.get_token().await?;
        let account_no = request.src_account_number.as_str();
        let src_acct = self.get_account_info(account_no).await?;

        let xfer = Transfer {
            src_acct,
            dest_acct_id: request.dst_account_number.clone(),
            amount: request.amount,
            metadata,
        };

        let req: XferReq = xfer.try_into()?;
        tracing::debug!(message = ?LoggingEvents::AttemptTransfer);
        let res = self.client_wrapper.transfer(&token, req).await?;
        tracing::debug!(message = ?LoggingEvents::TransferSucceeded);
        res.try_into()
    }

    #[tracing::instrument(skip(self))]
    pub async fn history(
        &self,
        account_no: &str,
        date_range: Option<DateRange>,
    ) -> Result<Vec<BankTransaction>> {
        let mcb_txns = self.history_mcb(account_no, date_range).await?;
        let bank_txns = mcb_txns.into_iter().map(Into::into).collect();
        Ok(bank_txns)
    }

    /// Queries for history for the given date range, and continues to fetch all pages until the
    /// cursor has been exhausted. Deserializes responses into the McbTransaction type
    pub async fn history_mcb(
        &self,
        account_no: &str,
        date_range: Option<DateRange>,
    ) -> Result<Vec<McbTransaction>> {
        let token = self.get_token().await?;
        let acct = self.get_account_info(account_no).await?;

        let hist = HistoryParams {
            acct,
            date_range,
            pagination_params: None,
        };

        // Make first request
        tracing::debug!(message = ?LoggingEvents::GettingHistory);
        let raw_history_resp = self
            .client_wrapper
            .history(&token, (&hist).try_into()?)
            .await?;

        // Extract transactions and cursor
        let (mut bank_txns, cursor) = raw_history_resp.get_page_and_cursor()?;

        // Query pages of history, moving cursor forward until complete
        bank_txns.extend(self.history_with_cursor(hist, cursor).await?);
        tracing::debug!(message = ?LoggingEvents::HistoryRetrieved);

        Ok(bank_txns)
    }

    /// Recursively query if cursor is Some, else return empty vec.
    async fn history_with_cursor(
        &self,
        history_params: HistoryParams,
        cursor: Option<McbCursor>,
    ) -> Result<Vec<McbTransaction>> {
        let token = self.get_token().await?;

        let mut all_txns = vec![];
        let mut curr_cursor = cursor;
        loop {
            if let Some(value) = curr_cursor {
                let pagination_params = PaginationParams { cursor: value };
                let mut history_params = history_params.clone();
                history_params.pagination_params = Some(pagination_params);

                let raw_history_resp = self
                    .client_wrapper
                    .history(&token, (&history_params).try_into()?)
                    .await?;
                let (mut current_page_txns, maybe_next_cursor) =
                    raw_history_resp.get_page_and_cursor()?;
                all_txns.append(&mut current_page_txns);
                curr_cursor = maybe_next_cursor;
            } else {
                return Ok(all_txns);
            }
        }
    }

    async fn get_token(&self) -> Result<Secret<String>> {
        tracing::debug!(message = ?LoggingEvents::GettingToken);
        let token = self.token_manager.get_or_refresh_locked().await?;
        tracing::debug!(message = ?LoggingEvents::RetrievedToken);
        Ok(token)
    }

    async fn get_account_info(&self, account_no: &str) -> Result<AccountInfo> {
        tracing::debug!(message = ?LoggingEvents::GettingAccountInfo);
        let acct = self
            .account_info_manager
            .get_account_info(account_no)
            .await?;
        tracing::debug!(message = ?LoggingEvents::AccountInfoRetrieved);
        Ok(acct)
    }
}
