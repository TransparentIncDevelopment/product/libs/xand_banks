use handlebars::Handlebars;

use mcb_auth_gen::models::AccessTokenOutput;

use crate::banks::mcb_adapter::{
    access_token_manager::AccessTokenManager, access_token_manager::RefreshPolicy,
    test::mock_raw_client::MockMcbRawClient, McbAdapter,
};

pub mod component_builders {
    use super::*;
    use crate::banks::mcb_adapter::account::McbAccountInfoManager;
    use crate::banks::mcb_adapter::client_wrapper::{AuthClientWrapper, DataClientWrapper};
    use crate::banks::mcb_adapter::config::McbClientTokenRetrievalAuthConfig;
    use crate::banks::mcb_adapter::test::util::response_builders::get_default_token_response;
    use crate::mocks::fake_secret_store::FakeSecretStore;
    use futures::executor::block_on;
    use pseudo::Mock;
    use std::sync::Arc;
    use std::time::Duration;
    use xand_secrets::SecretKeyValueStore;

    pub const TOKEN_EXPIRATION_TIME: i32 = 60;
    pub const POLICY_REFRESH_TIME: Duration = Duration::from_secs(60 * 5);

    const TEST_SECRET_KEY_USERNAME: &str = "fake/secrets/path/api_key_id";
    const TEST_SECRET_KEY_PASSWORD: &str = "fake/secrets/path/api_key_value";
    const TEST_USERNAME: &str = "username_value";
    const TEST_PASSWORD: &str = "password_value";

    /// Util function to construct an MCB adapter given a client manager (mock or real)
    pub fn build_adapter_with<
        C: DataClientWrapper,
        A: AuthClientWrapper,
        I: McbAccountInfoManager,
    >(
        client_wrapper: C,
        token_manager: AccessTokenManager<A>,
        info_manager: I,
    ) -> McbAdapter<C, A, I> {
        McbAdapter::new(client_wrapper, token_manager, info_manager)
    }

    /// Returns default mock test client. All http calls are errors by default
    pub fn get_mock_raw_client() -> MockMcbRawClient {
        MockMcbRawClient {
            get_auth_token: Mock::new(Ok(Arc::new(get_default_token_response()))),
            ..MockMcbRawClient::default()
        }
    }

    /// Default refresh policy (5min) with None token
    pub fn get_default_access_token_manager<C: AuthClientWrapper>(
        client: C,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> AccessTokenManager<C> {
        let policy = get_default_token_refresh_policy();
        let auth_config = get_default_auth_config();
        AccessTokenManager::new(policy, auth_config, secret_store, client)
    }

    /// Default refresh policy for testing is "refresh if expiring within 5 minutes"
    pub fn get_default_token_refresh_policy() -> RefreshPolicy {
        RefreshPolicy::PriorToTokenExpiration(POLICY_REFRESH_TIME)
    }

    pub fn get_default_auth_config() -> McbClientTokenRetrievalAuthConfig {
        McbClientTokenRetrievalAuthConfig {
            secret_key_username: String::from(TEST_SECRET_KEY_USERNAME),
            secret_key_password: String::from(TEST_SECRET_KEY_PASSWORD),
        }
    }

    pub fn get_default_fake_secret_store() -> Arc<FakeSecretStore> {
        let secret_store = FakeSecretStore::default();
        block_on(secret_store.add_secret(TEST_SECRET_KEY_USERNAME, TEST_USERNAME));
        block_on(secret_store.add_secret(TEST_SECRET_KEY_PASSWORD, TEST_PASSWORD));
        Arc::new(secret_store)
    }
}

pub mod response_builders {
    use super::*;
    use crate::models::{BankTransaction, BankTransactionType};
    use mcb_acct_gen::models::{AcctBal, AcctInqRs};
    use mcb_acct_trn_gen::models::{AcctTrnInqRs, AcctTrnRec};
    use mcb_transfer_gen::models::XferRes;

    /// Default token expires in 24 hours, with value of "secretToken"
    pub fn get_default_token_response() -> AccessTokenOutput {
        let mut token_response = AccessTokenOutput::new();
        token_response.access_token = Some("secretToken".to_string());
        token_response.expires_in = Some(86400);
        token_response.token_type = Some("Bearer".to_string());
        token_response
    }

    /// JSON sample response that has template-able variables for items we care about
    pub fn build_default_balance_resp(cur_bal: String, avail_bal: String) -> AcctInqRs {
        let registry = Handlebars::new();

        // Setup template variables
        let template_vars = json!({
            "CUR_BAL_AMT": cur_bal,
            "AVAIL_BAL_AMT": avail_bal
        });

        // Load template
        let balance_resp_template =
            include_str!("response_templates/sample_balance_resp_template.json");

        let json_balance_resp_str = registry
            .render_template(balance_resp_template, &template_vars)
            .unwrap();

        // Replace with given current balance and available balance
        let acct_inq_rs: AcctInqRs = serde_json::from_str(&json_balance_resp_str).unwrap();
        acct_inq_rs
    }

    /// Utility function to return a mutable reference to the balance vec buried inside the
    /// Account Inquiry response struct
    pub fn get_bal_vec_from_resp(balance_resp: &mut AcctInqRs) -> &mut Option<Vec<AcctBal>> {
        &mut balance_resp
            .acct_inq_rs
            .as_mut()
            .unwrap()
            .acct_rec
            .as_mut()
            .unwrap()
            .acct_info
            .as_mut()
            .unwrap()
            .acct_bal
    }

    pub fn build_default_transfer_resp(
        src_acct: &str,
        status: &str,
        confirmation_number: &str,
    ) -> XferRes {
        let template_vars = json!({
            "SOURCE_ACCT": src_acct,
            "STATUS": status,
            "CONFIRMATION_NUMBER": confirmation_number
        });

        // Load template as str
        let template = include_str!("response_templates/sample_transfer_resp_template.json");

        let registry = Handlebars::new();
        let json_transfer_resp_str = registry.render_template(template, &template_vars).unwrap();

        let transfer_resp: XferRes = serde_json::from_str(&json_transfer_resp_str).unwrap();
        transfer_resp
    }

    pub fn build_default_history_resp(
        acct: &str,
        test_txn_vec: Vec<BankTransaction>,
        cursor: Option<&str>,
    ) -> AcctTrnInqRs {
        // Load templates
        let mut registry = Handlebars::new();
        // Register a pass through escape function so it does not url encode quotations
        registry.register_escape_fn(|s| s.to_string());

        let history_resp_template =
            include_str!("response_templates/sample_history_resp_template.json");
        let txn_obj_template =
            include_str!("response_templates/sample_history_txn_obj_template.json");

        // Set up cursor value
        let cursor_str = match cursor {
            None => "null".to_string(), // No quotes around "null" since that's how it should come through in json
            Some(val) => format!("\"{}\"", val), // Add quotes around the val to sub in
        };

        let mut txn_vec = vec![];
        for test_txn in test_txn_vec {
            let txn_template_vars = json!({
                "TXN_ID": test_txn.bank_unique_id,
                "ACCOUNT_ID": acct,
                "METADATA": test_txn.metadata,
                "AMOUNT": test_txn.amount,
                "TXN_TYPE": transaction_type_to_mcb_string(test_txn.txn_type)
            });

            let txn_str = registry
                .render_template(txn_obj_template, &txn_template_vars)
                .unwrap();

            let txn: AcctTrnRec = serde_json::from_str(&txn_str).unwrap();
            txn_vec.push(txn);
        }

        let history_resp_vars = json!({
            "TXN_COUNT": txn_vec.len(),
            "CURSOR": cursor_str
        });

        let history_resp_str = registry
            .render_template(history_resp_template, &history_resp_vars)
            .unwrap();
        let mut history_resp: AcctTrnInqRs = serde_json::from_str(&history_resp_str).unwrap();
        history_resp.acct_trn_inq_rs.as_mut().unwrap().acct_trn_rec = Some(txn_vec);

        history_resp
    }

    fn transaction_type_to_mcb_string(txn_type: BankTransactionType) -> String {
        match txn_type {
            BankTransactionType::Credit => "Credit".to_string(),
            BankTransactionType::Debit => "Debit".to_string(),
        }
    }
}
