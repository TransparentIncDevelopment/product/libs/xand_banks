use crate::banks::mcb_adapter::test::mock_raw_client::MockAccountInfoManager;
use crate::banks::mcb_adapter::test::util::component_builders::{
    build_adapter_with, get_default_access_token_manager, get_default_fake_secret_store,
    get_mock_raw_client,
};
use crate::banks::mcb_adapter::test::util::response_builders::{
    build_default_balance_resp, build_default_history_resp, build_default_transfer_resp,
};
use crate::banks::mcb_adapter::LoggingEvents;
use crate::models::account::TransferRequest;
use crate::models::{BankTransaction, BankTransactionType};
use futures::executor::block_on;
use std::sync::Arc;
use tracing_assert_core::debug_fmt_ext::DebugFmtExt;
use tracing_assert_macros::tracing_capture_event_fields;
use xand_money::{Money, Usd};

#[test]
fn get_balance__can_find_traces() {
    // Build mock balance response
    let cur_bal = "100.25".to_string();
    let avail_bal = "99.25".to_string();
    let balance_resp = build_default_balance_resp(cur_bal, avail_bal);
    let mock_balance_resp = Ok(Arc::new(balance_resp));

    // Build mock MCBAdapter
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client
        .get_account_balance
        .return_value(mock_balance_resp);
    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(mock_mcb_client, token_manager, mock_account_info_manager);

    let account_no = "111111";
    let events = tracing_capture_event_fields!({
        block_on(mcb_adapter.get_balance(account_no)).unwrap();
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvents::GettingAccountBalance.debug_fmt(),
    )];
    assert!(events.contains(&expected_event));
}

#[test]
fn transfer__can_find_traces() {
    // Build mock transfer response
    let src_acct = "1111111111";
    let status = "Valid";
    let confirmation_number = "848494";

    let transfer_resp = build_default_transfer_resp(src_acct, status, confirmation_number);
    let mock_transfer_resp = Ok(Arc::new(transfer_resp));

    // Build mock MCBAdapter
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client.transfer.return_value(mock_transfer_resp);
    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(mock_mcb_client, token_manager, mock_account_info_manager);

    // Submit transfer request
    let dest_acct = "2222222222";
    let amt = Usd::from_f64_major_units(725).unwrap();
    let metadata = Some("doing a cool transfer".to_string());

    let transfer = TransferRequest::new(src_acct, dest_acct, amt);

    let events = tracing_capture_event_fields!({
        block_on(mcb_adapter.transfer(transfer.clone(), metadata)).unwrap();
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvents::AttemptTransfer.debug_fmt(),
    )];
    assert!(events.contains(&expected_event));
}

#[test]
fn history__can_find_traces() {
    // Build mock history response
    let account_no = "1111111111";
    let test_txn_vec: Vec<BankTransaction> = vec![
        BankTransaction {
            bank_unique_id: String::from("1234"),
            amount: Usd::from_i64_minor_units(1025).unwrap(),
            metadata: "metadata1".to_string(),
            txn_type: BankTransactionType::Credit,
        },
        BankTransaction {
            bank_unique_id: String::from("5678"),
            amount: Usd::from_i64_minor_units(1125).unwrap(),
            metadata: "metadata2".to_string(),
            txn_type: BankTransactionType::Debit,
        },
    ];

    let history_resp = build_default_history_resp(account_no, test_txn_vec, None);
    let mock_history_resp = Ok(Arc::new(history_resp));

    // Build mock MCBAdapter
    let mock_mcb_client = get_mock_raw_client();
    mock_mcb_client.history.return_value(mock_history_resp);
    let token_manager =
        get_default_access_token_manager(mock_mcb_client.clone(), get_default_fake_secret_store());
    let mock_account_info_manager = MockAccountInfoManager {};
    let mcb_adapter = build_adapter_with(mock_mcb_client, token_manager, mock_account_info_manager);

    // Submit history request
    let events = tracing_capture_event_fields!({
        block_on(mcb_adapter.history(account_no, None)).unwrap();
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvents::GettingHistory.debug_fmt(),
    )];
    assert!(events.contains(&expected_event));
}
