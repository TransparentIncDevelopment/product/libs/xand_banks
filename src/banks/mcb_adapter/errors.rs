use crate::banks::errors::BankErrors;
use crate::errors::XandBanksErrors;
use snafu::Snafu;
use std::sync::Arc;
use xand_money::MoneyError as XandMoneyError;
use xand_secrets::ReadSecretError;

pub type Result<T> = std::result::Result<T, McbHttpError>;

#[derive(Clone, Debug, Serialize, Snafu)]
#[snafu(visibility(pub))]
pub enum McbError {
    #[snafu(display("Mcb Http Error: {}", source))]
    HttpError { source: McbHttpError },
}

impl From<McbError> for XandBanksErrors {
    fn from(source: McbError) -> Self {
        XandBanksErrors::BankError {
            source: BankErrors::Mcb { source },
        }
    }
}

impl From<McbHttpError> for XandBanksErrors {
    fn from(source: McbHttpError) -> Self {
        XandBanksErrors::BankError {
            source: BankErrors::Mcb {
                source: source.into(),
            },
        }
    }
}

impl From<McbHttpError> for McbError {
    fn from(source: McbHttpError) -> Self {
        McbError::HttpError { source }
    }
}

#[derive(Clone, Debug, Serialize, Snafu)]
#[snafu(visibility(pub))]
pub enum McbHttpError {
    #[snafu(display("Reqwest Error: {}", source))]
    Reqwest {
        #[snafu(source(from(reqwest::Error, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<reqwest::Error>,
    },
    #[snafu(display("Serde Error: {}", source))]
    Serde {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<serde_json::Error>,
    },
    #[snafu(display("Io Error: {}", source))]
    Io {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<std::io::Error>,
    },
    #[snafu(display(
        "Http response error. Status code {}, content: \"{}\"",
        status_code,
        content
    ))]
    Response {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        status_code: reqwest::StatusCode,
        content: String,
    },
    #[snafu(display("Message: {}", message))]
    IncompleteMcbHttpResponseBody { message: String },
    #[snafu(display("Error parsing MCB transaction type. Value: {}", value))]
    ParseTransactionTypeError { value: String },
    #[snafu(display("Error parsing datetime. Input: {}, Error: {}", input, source))]
    ParseDatetimeError {
        #[snafu(source(from(chrono::ParseError, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<chrono::ParseError>,
        input: String,
    },
    #[snafu(display("Casting Error. Message: {} Source Error: {}", message, source))]
    CastError {
        #[snafu(source(from(std::num::TryFromIntError, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<std::num::TryFromIntError>,
        message: String,
    },
    #[snafu(display("Token Error: {}", source))]
    TokenBorrowMutError {
        #[snafu(source(from(std::cell::BorrowMutError, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<std::cell::BorrowMutError>,
    },

    #[snafu(display("Unexpected BalTypeSource value. Input: {}", input))]
    BalTypeError {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        input: String,
    },
    #[cfg(test)]
    #[snafu(display("Mock McbHttpError"))]
    Mock {},
    #[snafu(display("Problem converting Money: {}", source))]
    MoneyError {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: XandMoneyError,
    },
    #[snafu(display("Secret store lookup error: {}", source))]
    SecretStoreLookupError {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<ReadSecretError>,
    },
}

impl From<XandMoneyError> for McbHttpError {
    fn from(source: XandMoneyError) -> Self {
        Self::MoneyError { source }
    }
}

impl From<ReadSecretError> for McbHttpError {
    fn from(source: ReadSecretError) -> Self {
        Self::SecretStoreLookupError {
            source: Arc::new(source),
        }
    }
}

impl From<reqwest::Error> for McbHttpError {
    fn from(source: reqwest::Error) -> Self {
        Self::Reqwest {
            source: Arc::new(source),
        }
    }
}
