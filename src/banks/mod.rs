pub mod adapter_config;
pub mod bank_certification_tests;
pub mod config;
pub mod errors;
pub mod mcb_adapter;
pub mod treasury_prime_adapter;

use crate::date_range::DateRange;
use crate::models::{BankBalance, BankTransaction, BankTransferResponse};
use crate::{Bank, BankAdapter, Result};
use snafu::OptionExt;
use std::collections::HashMap;

use crate::errors::AccountNotFound;
use crate::models::account::{Account, DetailedAccount, TransferRequest};
use async_trait::async_trait;

#[derive(Clone)]
/// The XandBank struct is a wrapper around the more complicated inner workings of our bank adapters.
/// XandBank struct handles the easy, domain-specific setting and getting of display name, routing
/// number, trust account number and associated member accounts.
pub struct XandBank {
    pub name: String,
    pub adapter: Box<dyn BankAdapter>,
    pub routing_number: String,
    pub trust_account: Account,
    pub accounts: HashMap<String, Account>,
}

#[async_trait]
impl Bank for XandBank {
    fn routing_number(&self) -> String {
        self.routing_number.clone()
    }

    /// Return the reserve account id associated with this bank.
    fn reserve_account(&self) -> String {
        self.trust_account.account_number.clone()
    }

    fn accounts(&self) -> Vec<DetailedAccount> {
        let mut borrowables = vec![];
        for account_id in self.accounts.keys() {
            // This can't panic since we are iterating over our known accounts
            borrowables.push(self.get_enriched_account(account_id).unwrap())
        }
        borrowables
    }

    fn get_enriched_account(&self, account_id: &str) -> Result<DetailedAccount> {
        let account = self.accounts.get(account_id).context(AccountNotFound {
            account_id: account_id.to_string(),
        })?;

        Ok(DetailedAccount {
            id: account_id.to_string(),
            short_name: account.short_name.clone(),
            account_number: account.account_number.clone(),
            routing_number: self.routing_number(),
            bank_name: self.display_name(),
        })
    }

    /// Return this bank's display name.
    fn display_name(&self) -> String {
        self.name.clone()
    }

    fn memo_char_limit(&self) -> u32 {
        self.adapter.memo_char_limit()
    }

    #[tracing::instrument(skip(self))]
    async fn balance(&self, account_number: &str) -> Result<BankBalance> {
        self.adapter.balance(account_number).await
    }

    #[tracing::instrument(skip(self))]
    async fn transfer(
        &self,
        request: TransferRequest,
        metadata: Option<String>,
    ) -> Result<BankTransferResponse> {
        self.adapter.transfer(request, metadata).await
    }

    /// Given an account id, return the history for that account.
    #[tracing::instrument(skip(self))]
    async fn history(
        &self,
        account_number: &str,
        date_range: DateRange,
    ) -> Result<Vec<BankTransaction>> {
        self.adapter.history(account_number, date_range).await
    }
}

#[cfg(test)]
mod bank_tests {
    use super::*;
    use crate::errors::XandBanksErrors::AccountNotFound;
    use crate::mocks::mock_bank_adapter::MockBankAdapter;

    fn bank_builder() -> XandBank {
        let mut bank = XandBank {
            name: "Hyrule Credit Union".to_string(),
            adapter: MockBankAdapter::default().boxed(),
            routing_number: "whatev".to_string(),
            trust_account: Default::default(),
            accounts: Default::default(),
        };
        bank.accounts.insert(
            "link".to_string(),
            Account {
                id: "link".to_string(),
                short_name: "Link".to_string(),
                account_number: "7185657367".to_string(),
            },
        );

        bank.accounts.insert(
            "zelda".to_string(),
            Account {
                id: "zelda".to_string(),
                short_name: "Zelda the Princess".to_string(),
                account_number: "7185657367".to_string(),
            },
        );

        bank
    }

    #[test]
    fn unknown_account_returns_err() {
        let bank = bank_builder();

        let res = bank.get_enriched_account("joe exotic");

        match res.err().unwrap() {
            AccountNotFound { account_id } => assert_eq!(account_id, "joe exotic"),
            _ => panic!("Got unexpected error!"),
        }
    }

    #[test]
    fn creates_correct_enriched_accounts() {
        let bank = bank_builder();

        let enriched_account = bank.get_enriched_account("zelda").unwrap();

        assert_eq!(enriched_account.routing_number, bank.routing_number());
        assert_eq!(enriched_account.account_number, "7185657367");
        assert_eq!(enriched_account.id, "zelda");
        assert_eq!(enriched_account.short_name, "Zelda the Princess");
        assert_eq!(enriched_account.bank_name, "Hyrule Credit Union");
    }
}
