use super::mcb_adapter::errors::McbError;
use super::treasury_prime_adapter::errors::TreasuryPrimeError;
use snafu::Snafu;

#[derive(Clone, Debug, Serialize, Snafu)]
#[snafu(visibility(pub))]
pub enum BankErrors {
    #[snafu(display("{}", source))]
    Mcb { source: McbError },

    #[snafu(display("{:?}", source))]
    TreasuryPrime { source: TreasuryPrimeError },
}
