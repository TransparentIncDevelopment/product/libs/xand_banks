# Treasury Prime Bank adapter

Treasury Prime is a 3rd party tech company trying to modernize banking. They adapt backend bank infrastructure to a common API, and they provide the API interface to **Provident Bank**.

This crate (Treasury Prime Adapter) adapts the TreasuryPrime API to the Xand Bank trait. This trait is used by the Xand network to interoperate with each bank. That is, each bank must have an implementation of the Bank trait.

## Bank Trait

The Xand Bank Trait has methods to get account balance, create transfers between accounts, and retrieve transaction history. The Treasury Prime adapter implements all the methods on this trait:
```rust
pub trait Bank: dyn_clone::DynClone {
    fn routing_number(...) -> String;
    fn balance(...) -> Result<BankBalance>;
    fn transfer(...) -> Result<BankTransferResponse>;
    fn history(...) -> Result<Vec<BankTransaction>>;
    fn reserve_account(...) -> String;
    fn display_name(...) -> String;
}
```

## Configuration
 The adapter requires configuration about the bank API and trust account.  Provident needs its own config
 section in the Banks.toml for projects: **member-api**, and **trustee-node**. The configuration below contains example data.
 The real values are secured in Vault and 1Password.
 ```toml
[banks.provident]
bank_api_url = "http://localhost:8888/provident/"
routing_number = "211374020"
trust_account = "9999999999"
api_key_id = "replace-me" 
api_key_value = "replace-me" 
display_name = "Provident"
```
Purpose of each value:

* **bank_api_url:** Treasury Prime base url.
* **routing_number:** Provident routing number. Also used in Accounts.toml to relate accounts to banks.
* **trust_account:** The trust's account number at Provident bank.
* **api_key_id:** The BasicAuth 'username' in each http request to the API.
* **api_key_value:** The BasicAuth 'password' in each http request to the API.
* **display_name:** Display name of the bank. Ultimately used by the Wallet for UI purposes.

## Generated client
To reduce development overhead, we decided to generate a Rust client for Treasury Prime using OpenSource tools. Specifically,
Swagger/OpenAPI. Transparent maintains a Swagger spec (location: <thermite repo>/generated/treasury_prime/treasury_api.yaml)
that describes the endpoints and schema we care about. 

An OpenAPI client generator is then ran against the Swagger spec which generates a Rust crate with code for all the endpoints
and schema. This client is used directly by our adapter. Nothing in the generated client should be edited by hand.
Instead, the Swagger spec should be updated, and the client re-generated using the `generate_client.sh` located in the
same folder as the Swagger spec.

## Treasury Prime API Overview
Treasury Prime uses RESTful design, and is implemented via HTTP verbs, routes, status codes, authentication, json, etc... 
Complete documentation can be found here: https://dashboard.treasuryprime.com/docs. Our adapter utilizes these
two endpoints: 

* `/account`: Used to lookup account balances, and to map account numbers to account IDs.
* `/book` (book transfers): Used to create transfers and to lookup transfer history.

#### Authentication
Only BasicAuth is supported and required.

#### Correlation id
The Xand Network requires banks transactions to store and associate metadata to transactions. This metadata is generated
by the Xand network, and is used to correlate create requests to transactions at a bank. Treasury Prime
has a convenient field on the the Book Transfer object called `userdata:`. It is an arbitrary Json object, controlled by
by the user, and limited to 1K in size when serialized. 

#### Account number
A note about Account **Number** vs Account **ID**. Before any account related operation can be executed on the API, the
account (id) is fetched using account (number). This is because the API requires account (id) for many operations, but account
(number) is what we chose to store in our configuration, as members do not know their account (id).

#### Adapter implementation overview
The following is overview of how each method on the trait is implemented in the adapter.

* #### fn routing_number(...) -> String;
  Simply fetches the routing_number from the bank toml configuration

* #### fn reserve_account(...) -> String;
  Simply fetches trust_account number from the bank toml configuration

* #### fn display_name(...) -> String;
  Simply fetches display_name from the bank toml configuration

* #### fn balance(...) -> Result<BankBalance>;
  1. Account number is used to fetch account id.
  1. Account id is used to fetch account info which includes available balance, and current balance.

* #### fn transfer(...) -> Result<BankTransferResponse>;
  1. Account number is used to fetch account id for "From account".
  1. Account number is used to fetch account id for "To account".
  1. BookTransfer is POSTED using account ids, amount, and metadata (correlated id).

* #### fn history(...) -> Result<Vec<BankTransaction>>;
  1. Account number is used to fetch account id.
  1. From/To date are used to request first page of book transfers
  1. If results indicate more pages are available, then continue querying until all pages are retrieved
  1. All pages of transactions are concatenated, filtered, and returned.

