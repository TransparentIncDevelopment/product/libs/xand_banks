use crate::banks::errors::BankErrors;
use crate::banks::treasury_prime_adapter::client_wrapper::ClientAuthentication;
use crate::banks::treasury_prime_adapter::config::{TreasuryPrimeAuthConfig, TreasuryPrimeConfig};
use crate::banks::treasury_prime_adapter::errors::{Result, TreasuryPrimeError};
use crate::banks::treasury_prime_adapter::test_utils::fake_wrapper::{
    BookTransferPage, FakeClient, GetBookTransferRequest,
};
use crate::banks::treasury_prime_adapter::TreasuryPrimeAdapter;
use crate::constants::BANK_CALL_STANDARD_TIMEOUT;
use crate::date_range::DateRange;
use crate::errors::XandBanksErrors;
use crate::mocks::fake_secret_store::FakeSecretStore;
use crate::models::account::TransferRequest;
use crate::models::{BankBalance, BankTransaction, BankTransactionType, BankTransferResponse};
use crate::BankAdapter;
use chrono::{DateTime, Utc};
use futures::executor::block_on;
use std::sync::Arc;
use treasury_prime_client::models::{
    AccountDetail, AccountSummaries, AccountSummary, BookTransfer, BookTransferUserdata,
    BookTransfers,
};
use url::Url;
use xand_money::{Money, Usd};
use xand_secrets::{ExposeSecret, Secret};

const TEST_USERNAME_SECRET_STORE_LOOKUP_KEY: &str = "fake/secrets/path/api_key_id";
const TEST_PASSWORD_SECRET_STORE_LOOKUP_KEY: &str = "fake/secrets/path/api_key_value";
const TEST_USERNAME: &str = "username_value";
const TEST_PASSWORD: &str = "password_value";

pub fn standard_config() -> TreasuryPrimeConfig {
    TreasuryPrimeConfig {
        url: Url::parse("http://localhost").unwrap(),
        auth: TreasuryPrimeAuthConfig {
            secret_key_username: String::from(TEST_USERNAME_SECRET_STORE_LOOKUP_KEY),
            secret_key_password: String::from(TEST_PASSWORD_SECRET_STORE_LOOKUP_KEY),
        },
        timeout: BANK_CALL_STANDARD_TIMEOUT,
    }
}

pub fn create_fake_secret_store() -> Arc<FakeSecretStore> {
    let secret_store = FakeSecretStore::default();
    block_on(secret_store.add_secret(TEST_USERNAME_SECRET_STORE_LOOKUP_KEY, TEST_USERNAME));
    block_on(secret_store.add_secret(TEST_PASSWORD_SECRET_STORE_LOOKUP_KEY, TEST_PASSWORD));
    Arc::new(secret_store)
}

pub fn get_default_client_authentication() -> ClientAuthentication {
    get_basic_http_client_authentication(TEST_USERNAME, TEST_PASSWORD)
}

pub fn get_basic_http_client_authentication(
    username: &str,
    password: &str,
) -> ClientAuthentication {
    ClientAuthentication {
        username: Secret::new(String::from(username)),
        password: Secret::new(String::from(password)),
    }
}

pub fn standard_treasury_prime_bank(
    secret_store: Arc<FakeSecretStore>,
) -> TreasuryPrimeAdapter<FakeClient> {
    let config = standard_config();
    let secret_store = secret_store;
    TreasuryPrimeAdapter::<FakeClient>::new(&config, secret_store).unwrap()
}

pub struct TreasuryPrimeScenario {
    pub balance_result: Option<std::result::Result<BankBalance, XandBanksErrors>>,
    pub bank: TreasuryPrimeAdapter<FakeClient>,
    pub transfer_result: Option<std::result::Result<BankTransferResponse, XandBanksErrors>>,
    pub history_result: Option<std::result::Result<Vec<BankTransaction>, XandBanksErrors>>,
    secret_store: Arc<FakeSecretStore>,
}

impl Default for TreasuryPrimeScenario {
    fn default() -> Self {
        let secret_store = create_fake_secret_store();
        Self {
            balance_result: None,
            bank: standard_treasury_prime_bank(secret_store.clone()),
            transfer_result: None,
            history_result: None,
            secret_store,
        }
    }
}

impl TreasuryPrimeScenario {
    pub fn default_from_secret_store(secret_store: FakeSecretStore) -> Self {
        let wrapped_secret_store = Arc::new(secret_store);
        Self {
            balance_result: None,
            bank: standard_treasury_prime_bank(wrapped_secret_store.clone()),
            transfer_result: None,
            history_result: None,
            secret_store: wrapped_secret_store,
        }
    }

    pub fn given_account_by_account_number_returns(
        &mut self,
        account_number: &str,
        accounts_result: Result<AccountSummaries>,
    ) -> &mut Self {
        self.bank
            .client()
            .account_summaries_by_account_number
            .insert(account_number.to_string(), accounts_result);
        self
    }

    pub fn given_there_are_no_accounts(&mut self) -> &mut Self {
        self.bank
            .client()
            .account_summaries_by_account_number
            .clear();
        self
    }

    pub fn given_account_number_maps_to_account_id(
        &mut self,
        account_number: &str,
        account_id: &str,
    ) -> &mut Self {
        let account_summaries_result = Ok(AccountSummaries {
            data: vec![AccountSummary {
                id: account_id.to_string(),
            }],
        });
        self.bank
            .client()
            .account_summaries_by_account_number
            .insert(account_number.to_string(), account_summaries_result);
        self
    }

    pub fn given_two_accounts_with_same_account_number(
        &mut self,
        account_number: &str,
    ) -> &mut Self {
        let fake_id1 = format!("{}_id1", account_number);
        let fake_id2 = format!("{}_id2", account_number);

        let account_summaries_result = Ok(AccountSummaries {
            data: vec![
                AccountSummary { id: fake_id1 },
                AccountSummary { id: fake_id2 },
            ],
        });

        self.bank
            .client()
            .account_summaries_by_account_number
            .insert(account_number.to_string(), account_summaries_result);

        self
    }

    pub fn given_account_details_throws_error(&mut self, error: TreasuryPrimeError) -> &mut Self {
        // Every result in test util results array is now transformed into an error
        for result in self.bank.client().account_results.values_mut() {
            *result = Err(error.clone());
        }
        self
    }

    pub fn given_account_summaries_for_account_number_throws_error(
        &mut self,
        account_number: &str,
        error: TreasuryPrimeError,
    ) -> &mut Self {
        self.bank
            .client()
            .account_summaries_by_account_number
            .insert(account_number.to_string(), Err(error));
        self
    }

    pub fn given_create_book_transfer_request_returns(
        &mut self,
        error: TreasuryPrimeError,
    ) -> &mut Self {
        self.bank.client().create_book_transfer_request_response = Err(error);
        self
    }

    pub fn given_account_number_with_balances(
        &mut self,
        account_number: &str,
        avail_bal: &str,
        current_bal: &str,
    ) -> &mut Self {
        let fake_id = format!("{}_id", account_number);
        self.given_account_number_maps_to_account_id(account_number, &fake_id);
        let account_detail_result = Ok(AccountDetail {
            available_balance: avail_bal.to_string(),
            current_balance: current_bal.to_string(),
        });
        self.bank
            .client()
            .account_results
            .insert(fake_id, account_detail_result);
        self
    }

    pub fn given_account_returns(
        &mut self,
        account_id: &str,
        account_result: Result<AccountDetail>,
    ) -> &mut Self {
        self.bank
            .client()
            .account_results
            .insert(account_id.to_string(), account_result);
        self
    }

    pub async fn given_book_transfer(
        &mut self,
        from: &str,
        to: &str,
        id: &str,
        amount: &str,
        metadata: Option<&str>,
        status: &str,
    ) -> &mut Self {
        self.bank
            .client()
            .book_transfer_history_responses
            .lock()
            .await
            .push(Ok(BookTransfers {
                data: vec![BookTransfer {
                    id: id.to_string(),
                    amount: amount.to_string(),
                    from_account_id: from.to_string(),
                    status: status.to_string(),
                    to_account_id: to.to_string(),
                    userdata: Some(Box::new(BookTransferUserdata {
                        metadata: metadata.map(|x| x.to_string()),
                    })),
                }],
                page_next: None,
            }));
        self
    }

    pub async fn given_book_transfer_for_account_id_but_without_userdata(
        &mut self,
        account_id: &str,
    ) -> &mut Self {
        self.bank
            .client()
            .book_transfer_history_responses
            .lock()
            .await
            .push(Ok(BookTransfers {
                data: vec![BookTransfer {
                    id: "book_id".to_string(),
                    amount: "1.00".to_string(),
                    from_account_id: "blah".to_string(),
                    status: "sent".to_string(),
                    to_account_id: account_id.to_string(),
                    userdata: None,
                }],
                page_next: None,
            }));
        self
    }

    pub async fn given_book_transfer_returns_error(
        &mut self,
        error: TreasuryPrimeError,
    ) -> &mut Self {
        self.bank
            .client()
            .book_transfer_history_responses
            .lock()
            .await
            .push(Err(error));

        self
    }

    // TODO remove created_at from gen client

    pub async fn given_book_transfer_page(&mut self, page: BookTransferPage) -> &mut Self {
        self.bank
            .client()
            .book_transfer_history_responses
            .lock()
            .await
            .insert(
                0,
                Ok(BookTransfers {
                    data: page
                        .book_transfers
                        .into_iter()
                        .map(|transfer| BookTransfer {
                            id: transfer.transfer_id,
                            amount: transfer.amount,
                            from_account_id: transfer.from_account_id,
                            status: transfer.status,
                            to_account_id: transfer.to_account_id,
                            userdata: Some(Box::new(BookTransferUserdata {
                                metadata: transfer.meta_data,
                            })),
                        })
                        .collect(),
                    page_next: page.next_page,
                }),
            );
        self
    }

    // TODO look into logging messages within testing - when items ignored

    pub async fn when_i_get_balance_for_account(&mut self, account_number: &str) -> &mut Self {
        self.balance_result = Some(self.bank.balance(account_number).await);
        self
    }

    pub async fn when_i_get_history_for(
        &mut self,
        account: &str,
        begin: &str,
        end: &str,
    ) -> &mut Self {
        let begin_date = DateTime::parse_from_rfc3339(begin)
            .unwrap()
            .with_timezone(&Utc);
        let end_date = DateTime::parse_from_rfc3339(end)
            .unwrap()
            .with_timezone(&Utc);
        self.history_result = Some(
            self.bank
                .history(account, DateRange::new(begin_date, end_date))
                .await,
        );
        self
    }

    pub fn then_i_get_a_transaction_item(
        &mut self,
        metadata: &str,
        amount: Usd,
        transaction_type: &str,
    ) -> &mut Self {
        for result in (self.history_result).as_ref().unwrap().as_ref().unwrap() {
            if result.metadata == metadata
                && Usd::into_major_units(result.amount) == Usd::into_major_units(amount)
                && result.txn_type == transaction_type.parse::<BankTransactionType>().unwrap()
            {
                return self;
            }
        }
        panic!(
            "Unable to find bank transaction: Metadata: {}, amount:{:?}, txn_type:{}\n\nFound:\n{:?}",
            metadata, amount, transaction_type, self.history_result
        )
    }

    pub fn then_history_txn_count_is(&mut self, expected_count: usize) -> &mut Self {
        assert_eq!(
            (self.history_result)
                .as_ref()
                .unwrap()
                .as_ref()
                .unwrap()
                .len(),
            expected_count
        );

        self
    }

    pub fn then_available_balance_is(&mut self, expected_available_balance: Usd) -> &mut Self {
        match &self.balance_result {
            Some(Ok(res)) => assert_eq!(
                Usd::into_major_units(res.available_balance),
                Usd::into_major_units(expected_available_balance)
            ),
            _ => panic!(
                "Unexpected available balance result {:?}",
                &self.balance_result
            ),
        }
        self
    }

    pub fn then_current_balance_is(&mut self, expected_current_balance: Usd) -> &mut Self {
        match &self.balance_result {
            Some(Ok(res)) => assert_eq!(
                Usd::into_major_units(res.current_balance),
                Usd::into_major_units(expected_current_balance)
            ),
            _ => panic!(
                "Unexpected current balance result {:?}",
                &self.balance_result
            ),
        }
        self
    }

    pub fn then_balance_returned_error_message(
        &mut self,
        expected_error_message: &str,
    ) -> &mut Self {
        match &self.balance_result {
            Some(Err(XandBanksErrors::BankError {
                source: BankErrors::TreasuryPrime { source },
            })) => assert_eq!(format!("{}", source), expected_error_message),
            _ => panic!("Unexpected error {:?}", &self.balance_result),
        }
        self
    }

    pub fn then_transfer_returned_error_message(
        &mut self,
        expected_error_message: &str,
    ) -> &mut Self {
        match &self.transfer_result {
            Some(Err(XandBanksErrors::BankError {
                source: BankErrors::TreasuryPrime { source },
            })) => assert_eq!(format!("{}", source), expected_error_message),
            _ => panic!("Unexpected error {:?}", &self.transfer_result),
        }
        self
    }

    pub fn then_balance_returned_error(&mut self) -> &mut Self {
        match &self.balance_result {
            Some(Err(XandBanksErrors::BankError {
                source:
                    BankErrors::TreasuryPrime {
                        source: TreasuryPrimeError::MoneyError { source: _error },
                    },
            })) => (),
            _ => panic!(),
        }
        self
    }

    pub async fn when_i_transfer_from_to_accounts(
        &mut self,
        request: TransferRequest,
        metadata: Option<String>,
    ) -> &mut Self {
        let result = Some(self.bank.transfer(request, metadata).await);
        self.transfer_result = result;
        self
    }

    pub async fn then_book_transfer_metadata_is(&mut self, metadata: &str) -> &mut Self {
        assert_eq!(
            self.bank
                .client
                .book_transfer_request
                .lock()
                .await
                .as_ref()
                .unwrap()
                .userdata
                .metadata,
            metadata.to_string()
        );
        self
    }

    pub async fn then_book_transfer_destination_account_id_is(&mut self, acct: &str) -> &mut Self {
        assert_eq!(
            self.bank
                .client
                .book_transfer_request
                .lock()
                .await
                .as_ref()
                .unwrap()
                .to_account_id,
            acct
        );
        self
    }

    pub async fn then_book_transfer_source_account_id_is(&mut self, acct: &str) -> &mut Self {
        assert_eq!(
            self.bank
                .client
                .book_transfer_request
                .lock()
                .await
                .as_ref()
                .unwrap()
                .from_account_id,
            acct
        );
        self
    }

    pub async fn then_book_transfer_amount_is(&mut self, amount: &str) -> &mut Self {
        assert_eq!(
            self.bank
                .client
                .book_transfer_request
                .lock()
                .await
                .as_ref()
                .unwrap()
                .amount,
            amount
        );
        self
    }

    pub async fn then_book_transfers_api_was_called_ntimes_with(
        &mut self,
        calls: Vec<GetBookTransferRequest>,
    ) -> &mut Self {
        {
            let actual_calls = self.bank.client().get_book_requests.lock().await;
            assert_eq!(actual_calls.len(), calls.len());

            for (actual, expected) in actual_calls.iter().zip(calls) {
                assert_eq!(actual.from_date, expected.from_date);
                assert_eq!(actual.to_date, expected.to_date);
                assert_eq!(actual.page_cursor, expected.page_cursor);
            }
        }

        self
    }

    pub fn then_history_returned_error_message(&mut self, expected_message: &str) -> &mut Self {
        if let Some(Err(XandBanksErrors::BankError {
            source: BankErrors::TreasuryPrime { source: error },
        })) = &self.history_result
        {
            assert_eq!(format!("{}", error), expected_message);
            return self;
        }

        panic!(
            "Expected error, but there was no error! Result: {:?}",
            &self.history_result
        );
    }

    pub fn then_history_returned_error(&mut self) -> &mut Self {
        match &self.history_result {
            Some(Err(XandBanksErrors::BankError {
                source:
                    BankErrors::TreasuryPrime {
                        source: TreasuryPrimeError::MoneyError { source: _error },
                    },
            })) => (),
            _ => panic!(),
        }
        self
    }

    fn assert_auth_equal(expected: &ClientAuthentication, actual: &ClientAuthentication) {
        let ClientAuthentication {
            username: expected_username,
            password: expected_password,
        } = expected;
        let ClientAuthentication {
            username: actual_username,
            password: actual_password,
        } = actual;

        assert_eq!(
            expected_username.expose_secret(),
            actual_username.expose_secret()
        );
        assert_eq!(
            expected_password.expose_secret(),
            actual_password.expose_secret()
        );
    }

    pub async fn then_all_requests_authenticated_with_at_least_one_request(
        &mut self,
        auth: &ClientAuthentication,
    ) -> &mut Self {
        {
            let auth_requests = self.bank.client().authentication_requests.lock().await;

            assert!(auth_requests.len() > 0);
            for actual_auth in auth_requests.iter() {
                Self::assert_auth_equal(auth, actual_auth);
            }
        }

        self
    }

    pub async fn when_i_change_my_basic_http_authentication(
        &mut self,
        username: &str,
        password: &str,
    ) -> &mut Self {
        {
            self.secret_store
                .add_secret(TEST_USERNAME_SECRET_STORE_LOOKUP_KEY, username)
                .await;
            self.secret_store
                .add_secret(TEST_PASSWORD_SECRET_STORE_LOOKUP_KEY, password)
                .await;
        }

        self
    }

    pub async fn then_the_ith_request_is_authenticated(
        &mut self,
        i: usize,
        auth: &ClientAuthentication,
    ) -> &mut Self {
        {
            let auth_requests = self.bank.client().authentication_requests.lock().await;
            if let Some(actual_auth) = auth_requests.get(i) {
                Self::assert_auth_equal(auth, actual_auth);
            } else {
                panic!(
                    "The passed request index is out of range. i: {}, num_requests: {}",
                    i,
                    auth_requests.len()
                );
            }
        }

        self
    }

    pub async fn then_there_are_n_authenticated_requests(&mut self, n: usize) -> &mut Self {
        {
            let auth_requests = self.bank.client().authentication_requests.lock().await;
            assert_eq!(n, auth_requests.len())
        }

        self
    }
}

pub mod fake_wrapper {
    use crate::banks::treasury_prime_adapter::client_wrapper::{
        ClientAuthentication, ClientConfiguration, ClientWrapper,
    };
    use crate::banks::treasury_prime_adapter::errors::{Result, TreasuryPrimeError};
    use std::collections::HashMap;
    use std::sync::Arc;
    use treasury_prime_client::models::{
        AccountDetail, AccountSummaries, BookTransfer, BookTransferRequest, BookTransfers,
    };

    use async_trait::async_trait;
    use futures::lock::Mutex;

    pub struct GetBookTransferRequest {
        pub from_date: String,
        pub to_date: String,
        pub page_cursor: Option<String>,
    }

    pub struct BookTransferPage {
        pub book_transfers: Vec<Transfer>,
        pub next_page: Option<String>,
    }

    pub struct Transfer {
        pub from_account_id: String,
        pub to_account_id: String,
        pub transfer_id: String,
        pub amount: String,
        pub status: String,
        pub meta_data: Option<String>,
    }

    #[derive(Clone)]
    pub struct FakeClient {
        pub config: Arc<ClientConfiguration>,
        pub account_results: HashMap<String, Result<AccountDetail>>,
        pub account_summaries_by_account_number: HashMap<String, Result<AccountSummaries>>,
        pub book_transfer_request: Arc<Mutex<Option<BookTransferRequest>>>,
        pub get_book_requests: Arc<Mutex<Vec<GetBookTransferRequest>>>,
        pub create_book_transfer_request_response: Result<BookTransfer>,
        pub book_transfer_history_responses: Arc<Mutex<Vec<Result<BookTransfers>>>>,
        pub authentication_requests: Arc<Mutex<Vec<ClientAuthentication>>>,
    }

    impl FakeClient {
        async fn push_to_authentication_requests(&self, auth: &ClientAuthentication) {
            let mut auth_requests = self.authentication_requests.lock().await;
            auth_requests.push(auth.clone());
        }
    }

    #[async_trait]
    impl ClientWrapper for FakeClient {
        fn new(config: ClientConfiguration) -> Result<Self> {
            Ok(Self {
                config: Arc::new(config),
                account_results: HashMap::new(),
                account_summaries_by_account_number: HashMap::new(),
                book_transfer_request: create_empty_book_transfer_request(),
                create_book_transfer_request_response: Ok(BookTransfer::new(
                    "1".to_string(),
                    "1".to_string(),
                    "1".to_string(),
                    "pending".to_string(),
                    "1".to_string(),
                )),
                book_transfer_history_responses: create_empty_book_transfer_history_responses(),
                get_book_requests: create_empty_get_book_transfer_requests(),
                authentication_requests: create_empty_authentication_requests(),
            })
        }

        async fn account(&self, auth: &ClientAuthentication, id: &str) -> Result<AccountDetail> {
            self.push_to_authentication_requests(auth).await;
            match self.account_results.get(id) {
                Some(account) => account.clone(),
                None => Err(TreasuryPrimeError::TreasuryPrimeClientError {
                    message: "Unable to result for given account id; \
                              make sure mock setup to receive account id"
                        .to_string(),
                }),
            }
        }

        /// If no accounts are mapped in the mock, mimic actual API and return empty object - fix this by setting a correct return
        async fn account_summaries_by_account_number(
            &self,
            auth: &ClientAuthentication,
            account_number: &str,
        ) -> Result<AccountSummaries> {
            self.push_to_authentication_requests(auth).await;
            match self.account_summaries_by_account_number.get(account_number) {
                Some(accounts) => accounts.clone(),
                None => Ok(AccountSummaries { data: vec![] }),
            }
        }

        async fn create_book_transfer_request(
            &self,
            auth: &ClientAuthentication,
            book_transfer_request: BookTransferRequest,
        ) -> Result<BookTransfer> {
            self.push_to_authentication_requests(auth).await;
            let mut transfer_request = self.book_transfer_request.lock().await;
            *transfer_request = Some(book_transfer_request);
            self.create_book_transfer_request_response.clone()
        }

        async fn history(
            &self,
            auth: &ClientAuthentication,
            from_date: &str,
            to_date: &str,
            page_cursor: Option<&str>,
        ) -> Result<BookTransfers> {
            self.push_to_authentication_requests(auth).await;
            let mut get_book_requests = self.get_book_requests.lock().await;
            get_book_requests.push(GetBookTransferRequest {
                from_date: from_date.to_string(),
                to_date: to_date.to_string(),
                page_cursor: page_cursor.map(|x| x.to_string()),
            });

            let mut responses = self.book_transfer_history_responses.lock().await;
            responses.pop().unwrap()
        }
    }

    fn create_empty_book_transfer_request() -> Arc<Mutex<Option<BookTransferRequest>>> {
        let book_transfer_mutex = Mutex::<Option<BookTransferRequest>>::new(None);
        Arc::<Mutex<Option<BookTransferRequest>>>::new(book_transfer_mutex)
    }

    fn create_empty_get_book_transfer_requests() -> Arc<Mutex<Vec<GetBookTransferRequest>>> {
        let get_book_transfers_mutex = Mutex::<Vec<GetBookTransferRequest>>::new(vec![]);
        Arc::<Mutex<Vec<GetBookTransferRequest>>>::new(get_book_transfers_mutex)
    }

    fn create_empty_book_transfer_history_responses() -> Arc<Mutex<Vec<Result<BookTransfers>>>> {
        let mutex = Mutex::<Vec<Result<BookTransfers>>>::new(vec![]);
        Arc::<Mutex<Vec<Result<BookTransfers>>>>::new(mutex)
    }

    fn create_empty_authentication_requests() -> Arc<Mutex<Vec<ClientAuthentication>>> {
        Arc::new(Mutex::new(vec![]))
    }
}
