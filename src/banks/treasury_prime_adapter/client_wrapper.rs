use crate::banks::treasury_prime_adapter::errors::{Result, TreasuryPrimeError};
use treasury_prime_client::apis::{
    account_api::account, account_api::account_summaries_by_account_number,
    configuration::Configuration as GeneratedConfiguration, history_api::get_book_transfers,
    transfer_api::create_book_transfer_request, Error,
};
use treasury_prime_client::models::{
    AccountDetail, AccountSummaries, BookTransfer, BookTransferRequest, BookTransfers,
};

use async_trait::async_trait;
use std::time::Duration;
use std::{fmt::Debug, sync::Arc};
use xand_secrets::{ExposeSecret, Secret};

pub struct ClientConfiguration {
    pub base_path: String,
    pub user_agent: Option<String>,
    pub timeout: Duration,
}

#[derive(Clone, Debug)]
pub struct ClientAuthentication {
    pub username: Secret<String>,
    pub password: Secret<String>,
}

#[async_trait]
pub trait ClientWrapper {
    fn new(config: ClientConfiguration) -> Result<Self>
    where
        Self: Sized;

    async fn account(&self, auth: &ClientAuthentication, id: &str) -> Result<AccountDetail>;

    async fn account_summaries_by_account_number(
        &self,
        auth: &ClientAuthentication,
        account_number: &str,
    ) -> Result<AccountSummaries>;

    async fn create_book_transfer_request(
        &self,
        auth: &ClientAuthentication,
        book_transfer_request: BookTransferRequest,
    ) -> Result<BookTransfer>;

    async fn history(
        &self,
        auth: &ClientAuthentication,
        from_date: &str,
        to_date: &str,
        page_cursor: Option<&str>,
    ) -> Result<BookTransfers>;
}

impl<T: Debug> From<Error<T>> for TreasuryPrimeError {
    fn from(error: Error<T>) -> Self {
        TreasuryPrimeError::TreasuryPrimeClientError {
            message: format!("{:?}", error),
        }
    }
}

#[derive(Clone)]
pub struct RealClient {
    config: Arc<ClientConfiguration>,
    client: reqwest::Client,
}

impl RealClient {
    fn merge_to_generated_config(&self, auth: &ClientAuthentication) -> GeneratedConfiguration {
        let ClientAuthentication { username, password } = auth;
        GeneratedConfiguration {
            base_path: self.config.base_path.clone(),
            user_agent: self.config.user_agent.clone(),
            client: self.client.clone(),
            basic_auth: Some((
                username.expose_secret().clone(),
                Some(password.expose_secret().clone()),
            )),
            oauth_access_token: None,
            bearer_access_token: None,
            api_key: None,
        }
    }
}

#[async_trait]
impl ClientWrapper for RealClient {
    fn new(config: ClientConfiguration) -> Result<Self> {
        reqwest::Client::builder()
            .timeout(config.timeout)
            .build()
            .map(|client| Self {
                config: Arc::new(config),
                client,
            })
            .map_err(|e| e.into())
    }

    async fn account(&self, auth: &ClientAuthentication, id: &str) -> Result<AccountDetail> {
        let config = self.merge_to_generated_config(auth);
        let account = account(&config, id).await?;
        Ok(account)
    }

    async fn account_summaries_by_account_number(
        &self,
        auth: &ClientAuthentication,
        account_number: &str,
    ) -> Result<AccountSummaries> {
        let config = self.merge_to_generated_config(auth);
        let accounts = account_summaries_by_account_number(&config, account_number).await?;
        Ok(accounts)
    }

    async fn create_book_transfer_request(
        &self,
        auth: &ClientAuthentication,
        book_transfer_request: BookTransferRequest,
    ) -> Result<BookTransfer> {
        let config = self.merge_to_generated_config(auth);
        create_book_transfer_request(&config, book_transfer_request)
            .await
            .map_err(|error| error.into())
    }

    async fn history(
        &self,
        auth: &ClientAuthentication,
        from_date: &str,
        to_date: &str,
        page_cursor: Option<&str>,
    ) -> Result<BookTransfers> {
        let config = self.merge_to_generated_config(auth);
        get_book_transfers(&config, from_date, to_date, page_cursor)
            .await
            .map_err(|err| err.into())
    }
}
