use crate::BankAdapter;
use crate::{
    banks::{
        errors::BankErrors,
        treasury_prime_adapter::{client_wrapper::ClientWrapper, errors::*},
    },
    date_range::DateRange,
    errors::XandBanksErrors,
    models::{BankBalance, BankTransaction, BankTransactionType, BankTransferResponse},
    Result as LibResult,
};
use chrono::SecondsFormat;
use itertools::Itertools;
use treasury_prime_client::models::{
    BookTransfer, BookTransferRequest, BookTransferRequestUserdata, BookTransferUserdata,
    BookTransfers,
};
use url::Url;
use xand_money::{Money, Usd};

use self::config::TreasuryPrimeAuthConfig;
use crate::models::account::TransferRequest;
use async_trait::async_trait;
use client_wrapper::ClientAuthentication;
use std::sync::Arc;
use xand_secrets::SecretKeyValueStore;

pub mod client_wrapper;
pub mod config;
pub mod errors;
#[cfg(test)]
pub mod test_utils;
#[cfg(test)]
pub mod tests;

/// This is a cloneable wrapper for the TreasuryPrimeAdapter to satisfy trait restrictions on BankDispatcher/Bank
#[derive(Clone)]
pub struct TreasuryPrimeAdapter<Client: ClientWrapper> {
    client: Client,
    api_auth_config: TreasuryPrimeAuthConfig,
    secret_store: Arc<dyn SecretKeyValueStore>,
}

const NEXT_PAGE_CURSOR_PARAMETER_NAME: &str = "page_cursor";

#[derive(Debug)]
enum LoggingEvents {
    GettingAuth,
    AuthRetrieved,
    GettingAccountID,
    AccountIDRetrieved,
    GettingBalance,
    BalanceRetrieved,
    AttemptTransfer,
    TransferSucceeded,
    GettingHistory,
    HistoryRetrieved,
}

impl<A: ClientWrapper + Clone + Send + Sync> TreasuryPrimeAdapter<A> {
    pub fn new(
        cfg: &config::TreasuryPrimeConfig,
        secret_store: Arc<dyn SecretKeyValueStore>,
    ) -> LibResult<Self> {
        let client = A::new(cfg.treasury_prime_client_config()?)?;
        Ok(TreasuryPrimeAdapter {
            client,
            api_auth_config: cfg.auth.clone(),
            secret_store,
        })
    }

    #[allow(dead_code)]
    fn client(&mut self) -> &mut A {
        &mut self.client
    }

    async fn retrieve_auth(&self) -> Result<ClientAuthentication> {
        tracing::debug!(message = ?LoggingEvents::GettingAuth);
        let TreasuryPrimeAuthConfig {
            secret_key_username,
            secret_key_password,
        } = &self.api_auth_config;
        let username = self.secret_store.read(secret_key_username.as_str()).await?;
        let password = self.secret_store.read(secret_key_password.as_str()).await?;
        tracing::debug!(message = ?LoggingEvents::AuthRetrieved);
        Ok(ClientAuthentication { username, password })
    }

    async fn account_id_from_account_number(&self, account_number: &str) -> LibResult<String> {
        tracing::debug!(message = ?LoggingEvents::GettingAccountID);

        let mut accounts = self
            .client
            .account_summaries_by_account_number(&self.retrieve_auth().await?, account_number)
            .await?
            .data;

        let account = match accounts.pop() {
            Some(account) => account,
            None => {
                return Err(TreasuryPrimeError::AccountNotFound {
                    account_number: account_number.to_string(),
                }
                .into())
            }
        };

        if !accounts.is_empty() {
            return Err(TreasuryPrimeError::MoreThanOneAccount {
                account_number: account_number.to_string(),
            }
            .into());
        }

        tracing::debug!(message = ?LoggingEvents::AccountIDRetrieved);
        Ok(account.id)
    }

    async fn paged_book_transfers(&self, date_range: DateRange) -> Result<Vec<BookTransfer>> {
        let begin_date = date_range
            .begin_date()
            .to_rfc3339_opts(SecondsFormat::Secs, true);
        let end_date = date_range
            .end_date()
            .to_rfc3339_opts(SecondsFormat::Secs, true);
        let mut book_transfers = vec![];
        let mut cursor: Option<String> = None;

        loop {
            let mut page_result = self
                .get_transactions(&begin_date, &end_date, cursor.as_deref())
                .await?;

            book_transfers.append(&mut page_result.data);

            let next_page_url = match page_result.page_next {
                Some(next_page_url) => next_page_url,
                _ => break,
            };
            cursor = Some(self.next_cursor(next_page_url)?);
        }

        Ok(book_transfers)
    }

    async fn get_transactions(
        &self,
        begin_date: &str,
        end_date: &str,
        cursor: Option<&str>,
    ) -> Result<BookTransfers> {
        self.client
            .history(&self.retrieve_auth().await?, begin_date, end_date, cursor)
            .await
    }

    fn next_cursor(&self, next_page_url: String) -> Result<String> {
        Url::parse(&next_page_url)
            .map_err(|_| TreasuryPrimeError::UrlParseError {
                url: next_page_url.clone(),
            })?
            .query_pairs()
            .into_owned()
            .filter(|query| query.0 == NEXT_PAGE_CURSOR_PARAMETER_NAME)
            .map(|query| query.1)
            .exactly_one()
            .map_err(|_| TreasuryPrimeError::UrlParseError { url: next_page_url })
    }
}

impl From<TreasuryPrimeError> for XandBanksErrors {
    fn from(source: TreasuryPrimeError) -> Self {
        XandBanksErrors::BankError {
            source: BankErrors::TreasuryPrime { source },
        }
    }
}

#[async_trait]
impl<A: ClientWrapper + Clone + Send + Sync> BankAdapter for TreasuryPrimeAdapter<A> {
    #[tracing::instrument(skip(self))]
    async fn balance(&self, account_number: &str) -> LibResult<BankBalance> {
        let auth = self.retrieve_auth().await?;
        let account_id = self.account_id_from_account_number(account_number).await?;
        tracing::debug!(message = ?LoggingEvents::GettingBalance);
        let account = self.client.account(&auth, &account_id).await?;
        let available_balance = Usd::from_str(account.available_balance)?;
        let current_balance = Usd::from_str(account.current_balance)?;
        tracing::debug!(message = ?LoggingEvents::BalanceRetrieved);

        Ok(BankBalance {
            available_balance,
            current_balance,
        })
    }

    #[tracing::instrument(skip(self))]
    async fn transfer(
        &self,
        request: TransferRequest,
        metadata: Option<String>,
    ) -> LibResult<BankTransferResponse> {
        let from_acct_id = self
            .account_id_from_account_number(request.src_account_number.as_str())
            .await?;
        let to_acct_id = self
            .account_id_from_account_number(request.dst_account_number.as_str())
            .await?;
        let corrected_amount = format!("{:.2}", request.amount.into_major_units());
        let auth = self.retrieve_auth().await?;

        tracing::debug!(message = ?LoggingEvents::AttemptTransfer);
        self.client
            .create_book_transfer_request(
                &auth,
                BookTransferRequest {
                    amount: corrected_amount,
                    description: None,
                    from_account_id: from_acct_id,
                    to_account_id: to_acct_id,
                    userdata: Box::new(BookTransferRequestUserdata {
                        metadata: metadata.ok_or(TreasuryPrimeError::RequestMissingMetadata {
                            src_acct_no: request.src_account_number,
                            dest_acct_no: request.dst_account_number,
                        })?,
                    }),
                },
            )
            .await?;

        tracing::debug!(message = ?LoggingEvents::TransferSucceeded);
        Ok(BankTransferResponse {
            message: "A book transfer was made".to_string(),
        })
    }

    #[tracing::instrument(skip(self))]
    async fn history(
        &self,
        acct_no: &str,
        date_range: DateRange,
    ) -> LibResult<Vec<BankTransaction>> {
        let acct_id = self.account_id_from_account_number(acct_no).await?;
        let book_transfers = self.paged_book_transfers(date_range).await?;

        tracing::debug!(message = ?LoggingEvents::GettingHistory);
        let history = book_transfers
            .into_iter()
            .filter(has_different_to_and_from_accounts)
            .filter(has_metadata)
            .filter(has_sent_status)
            .filter(|bt| is_for_account(bt, &acct_id))
            .map(|bt| create_bank_transaction(bt, &acct_id))
            .collect::<Result<Vec<BankTransaction>>>()
            .map_err(TreasuryPrimeError::into);
        tracing::debug!(message = ?LoggingEvents::HistoryRetrieved);
        history
    }

    fn memo_char_limit(&self) -> u32 {
        // Supports up to 1kb serialized JSON, so a rather large number here is fine
        100
    }
}

fn has_different_to_and_from_accounts(book_transfer: &BookTransfer) -> bool {
    book_transfer.from_account_id != book_transfer.to_account_id
}

fn has_metadata(book_transfer: &BookTransfer) -> bool {
    match &book_transfer.userdata {
        Some(userdata) => userdata.metadata.is_some(),
        None => false,
    }
}

fn is_for_account(book_transfer: &BookTransfer, account_id: &str) -> bool {
    book_transfer.from_account_id == account_id || book_transfer.to_account_id == account_id
}

fn has_sent_status(book_transfer: &BookTransfer) -> bool {
    book_transfer.status == "sent"
}

fn create_bank_transaction(
    book_transfer: BookTransfer,
    account_id: &str,
) -> Result<BankTransaction> {
    let amount = Usd::from_str(book_transfer.amount.clone())?;

    let metadata = match book_transfer.userdata.map(|x| *x) {
        Some(BookTransferUserdata {
            metadata: Some(metadata),
        }) => metadata,
        _ => String::new(),
    };

    let txn_type = if book_transfer.from_account_id == account_id {
        BankTransactionType::Debit
    } else {
        BankTransactionType::Credit
    };

    Ok(BankTransaction {
        bank_unique_id: book_transfer.id,
        amount,
        metadata,
        txn_type,
    })
}
