use crate::{
    banks::treasury_prime_adapter::test_utils::TreasuryPrimeScenario,
    banks::treasury_prime_adapter::LoggingEvents, models::account::TransferRequest,
};
use tokio::runtime::Runtime;
use tracing_assert_core::debug_fmt_ext::DebugFmtExt;
use tracing_assert_macros::tracing_capture_event_fields;
use xand_money::{Money, Usd};

#[test]
fn balance__can_find_traces() {
    let rt = Runtime::new().unwrap();

    let events = tracing_capture_event_fields!({
        rt.block_on(async {
            TreasuryPrimeScenario::default()
                .given_account_number_with_balances("account1", "5.04", "5.05")
                .when_i_get_balance_for_account("account1")
                .await;
        });
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvents::GettingBalance.debug_fmt(),
    )];
    assert!(events.contains(&expected_event));
}

#[test]
fn transfer__can_find_traces() {
    let rt = Runtime::new().unwrap();

    let events = tracing_capture_event_fields!({
        rt.block_on(async {
            TreasuryPrimeScenario::default()
                .given_account_number_maps_to_account_id("account1", "account1_id")
                .given_account_number_maps_to_account_id("account2", "account2_id")
                .when_i_transfer_from_to_accounts(
                    TransferRequest::new(
                        "account1",
                        "account2",
                        Usd::from_f64_major_units(3.00_f64).unwrap(),
                    ),
                    Some("some random metadata".to_string()),
                )
                .await;
        });
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvents::AttemptTransfer.debug_fmt(),
    )];
    assert!(events.contains(&expected_event));
}

#[test]
fn history__can_find_traces() {
    let rt = Runtime::new().unwrap();

    let events = tracing_capture_event_fields!({
        rt.block_on(async {
            TreasuryPrimeScenario::default()
                .given_account_number_maps_to_account_id("account1", "account1_id")
                .given_book_transfer(
                    "account1_id",
                    "account2_id",
                    "transfer_123",
                    "1.56",
                    Some("meta123"),
                    "sent",
                )
                .await
                .when_i_get_history_for("account1", "2020-01-04T10:39:57Z", "2020-01-05T10:39:57Z")
                .await;
        });
    });

    let expected_event: Vec<(String, String)> = vec![(
        "message".to_string(),
        LoggingEvents::GettingHistory.debug_fmt(),
    )];
    assert!(events.contains(&expected_event));
}
