use crate::errors::XandBanksErrors::BankError;
use crate::models::BankTransactionType;
use crate::{BankAdapter, BankBalance, DateRange, TransferRequest};
use async_trait::async_trait;
use chrono::{Duration, Utc};
use rust_decimal::Decimal;
use xand_money::{Money, Usd};

///This set of tests certifies the basic functionality of a given bank adapter
/// To use this you must provide an expression that returns a BankAdapterTestContext
/// To implement new tests implement them as a function that takes a BankAdapterTestContext
/// and add the call into the macro
#[macro_export]
macro_rules! bank_adapter_tests {
    ($context_generator:expr) => {
        use xand_banks::banks::bank_certification_tests::{
            fetch_balance_assert_ok, issue_transfer_and_assert_metadata,
            test_balance_changes_after_transfer, test_history_is_initially_empty,
            test_history_returns_debits_and_credits_for_account,
        };
        #[tokio::test]
        async fn history__is_initially_empty() {
            let adapter = $context_generator;
            test_history_is_initially_empty(adapter).await;
        }

        #[tokio::test]
        async fn balance__can_fetch() {
            let adapter = $context_generator;
            fetch_balance_assert_ok(adapter).await;
        }

        #[tokio::test]
        async fn transfer__balances_change() {
            let adapter = $context_generator;
            test_balance_changes_after_transfer(adapter).await;
        }

        #[tokio::test]
        async fn transfer__contains_metadata() {
            let context = $context_generator;
            issue_transfer_and_assert_metadata(context).await;
        }

        #[tokio::test]
        async fn history__returns_debits_and_credits_for_account() {
            let context = $context_generator;
            test_history_returns_debits_and_credits_for_account(context).await;
        }
    };
}

#[async_trait]
pub trait BankAdapterTestContext {
    type Adapter: BankAdapter;
    async fn get_adapter(&self) -> Self::Adapter;
}

pub async fn fetch_balance_assert_ok<T: BankAdapterTestContext>(context: T) {
    let account = "1";

    // When
    let balance = context.get_adapter().await.balance(account).await;

    // Then
    assert!(balance.is_ok());
}

pub async fn issue_transfer_and_assert_metadata<T: BankAdapterTestContext>(context: T) {
    let from_account = "1";
    let to_account = "2";

    let metadata = "abcd";
    let amount = Usd::from_i64_minor_units(100).unwrap();
    let transfer_request = TransferRequest {
        src_account_number: from_account.into(),
        dst_account_number: to_account.to_string(),
        amount,
    };
    let adapter = context.get_adapter().await;

    // When
    let _resp = adapter
        .transfer(transfer_request, Some(metadata.into()))
        .await
        .expect("Transfer failed");

    // Then
    let txns = adapter
        .history(from_account, get_history_date_range())
        .await
        .expect("Getting history failed");

    dbg!(&txns);
    assert_eq!(
        txns.get(0)
            .expect("Expected one transaction. Found none")
            .metadata,
        metadata
    );
}

pub async fn test_history_is_initially_empty<T: BankAdapterTestContext>(context: T) {
    // Given
    let adapter = context.get_adapter().await;
    let account_number = "1";
    let date_range = DateRange::from_past_to_now(Duration::days(1)).unwrap();

    // When
    let txns = adapter
        .history(account_number, date_range)
        .await
        .expect("Getting history failed");

    // Then
    assert_eq!(txns, vec![]);
}

pub async fn test_balance_changes_after_transfer<T: BankAdapterTestContext>(context: T) {
    // Given
    let adapter = context.get_adapter().await;

    let from_account = "1";
    let to_account = "2";

    let from_account_initial_balance = adapter.balance(from_account).await.unwrap();
    let amount = Usd::from_i64_minor_units(100).unwrap();
    let expected_final_balance = from_account_initial_balance
        .available_balance
        .into_major_units()
        - amount.into_major_units();
    let expected_final_balance = Usd::from_str(expected_final_balance.to_string()).unwrap();

    let transfer_request = TransferRequest {
        src_account_number: from_account.into(),
        dst_account_number: to_account.to_string(),
        amount,
    };

    // When
    let _resp = adapter
        .transfer(transfer_request, Some("metadata".into()))
        .await
        .expect("Transfer failed");

    // Then
    let from_account_balance = adapter
        .balance(from_account)
        .await
        .expect("Balance call failed");
    assert_eq!(
        from_account_balance,
        BankBalance {
            available_balance: expected_final_balance,
            current_balance: expected_final_balance
        }
    );
}

pub async fn test_history_returns_debits_and_credits_for_account<T: BankAdapterTestContext>(
    context: T,
) {
    // Given
    let adapter = context.get_adapter().await;

    let account = "1";
    let other_account = "2";

    let metadata = "abcd";

    // And a debit book transfer
    let debit_amount = Usd::from_i64_minor_units(100).unwrap();
    let debit = TransferRequest {
        src_account_number: account.into(),
        dst_account_number: other_account.to_string(),
        amount: debit_amount,
    };
    adapter
        .transfer(debit, Some(metadata.into()))
        .await
        .expect("Transfer failed");

    // And a credit book transfer
    let credit_amount = Usd::from_i64_minor_units(200).unwrap();
    let credit = TransferRequest {
        src_account_number: other_account.to_string(),
        dst_account_number: account.into(),
        amount: credit_amount,
    };
    adapter
        .transfer(credit, Some(metadata.into()))
        .await
        .expect("Transfer failed");

    // When
    let txns = adapter
        .history(account, get_history_date_range())
        .await
        .expect("Getting history failed");

    // Then
    let credit_txn = txns.get(0).expect("Expected two transactions found none");
    assert_eq!(credit_txn.amount.into_minor_units(), Decimal::from(200));
    assert_eq!(credit_txn.txn_type, BankTransactionType::Credit);

    let debit_txn = txns.get(1).expect("Expected two transactions found one");
    assert_eq!(debit_txn.amount.into_minor_units(), Decimal::from(100));
    assert_eq!(debit_txn.txn_type, BankTransactionType::Debit);
}

/// Treasury prime bank mocks tracks transactions timestamps at millisecond precision.
/// The end bounds of the DateRange in a history request are only second-precision.
/// If the history request is submitted in the same second as the book tranfser, the mocks
/// treat the end bound as having .000 milliseconds so they do not include the transfer.
/// To avoid sleeping we select a date range shifted into the future
fn get_history_date_range() -> DateRange {
    let now_plus_a_second = Utc::now() + Duration::seconds(1);
    let one_day_ago = now_plus_a_second - Duration::days(1);
    DateRange::new(one_day_ago, now_plus_a_second)
}

#[async_trait]
pub trait BankAdapterTimeoutTestContext {
    type Adapter: BankAdapter;
    async fn get_adapter_with_timeout(&self, timeout_in_seconds: u64) -> Self::Adapter;
    async fn set_latency(&self, latency_in_seconds: u64);
}

/// This set of tests certifies the timeout functionality of a given bank adapter
/// To use this you must provide an expression that returns a BankAdapterTimeoutTestContext
/// This requires that the test context can simulate arbitrary latency in the adapter
/// To implement new tests implement them as a function that takes a BankAdapterTimeoutTestContext
/// and add the call into the macro
#[macro_export]
macro_rules! bank_adapter_timeout_tests {
    ($context_generator:expr) => {
        use xand_banks::banks::bank_certification_tests::{
            test_balance_timeout_returns_error, test_history_timeout_returns_error,
            test_transfer_timeout_returns_error,
        };

        #[tokio::test]
        async fn balance__will_timeout() {
            let context = $context_generator;
            test_balance_timeout_returns_error(context).await;
        }

        #[tokio::test]
        async fn transfer__will_timeout() {
            let context = $context_generator;
            test_transfer_timeout_returns_error(context).await;
        }

        #[tokio::test]
        async fn history__will_timeout() {
            let context = $context_generator;
            test_history_timeout_returns_error(context).await;
        }
    };
}

pub async fn test_history_timeout_returns_error<T: BankAdapterTimeoutTestContext>(context: T) {
    let timeout = 1;
    let adapter = context.get_adapter_with_timeout(1).await;
    context.set_latency(timeout + 1).await;
    let account_number = "1";
    let date_range = DateRange::from_past_to_now(Duration::days(1)).unwrap();

    // When
    let res = adapter.history(account_number, date_range).await;

    assert!(matches!(res, Err(BankError { .. })));
}

pub async fn test_transfer_timeout_returns_error<T: BankAdapterTimeoutTestContext>(context: T) {
    let timeout = 1;
    let adapter = context.get_adapter_with_timeout(1).await;
    context.set_latency(timeout + 1).await;
    let from_account = "1";
    let to_account = "2";

    let metadata = "abcd";
    let amount = Usd::from_i64_minor_units(100).unwrap();
    let transfer_request = TransferRequest {
        src_account_number: from_account.into(),
        dst_account_number: to_account.to_string(),
        amount,
    };

    // When
    let res = adapter
        .transfer(transfer_request, Some(metadata.into()))
        .await;

    assert!(matches!(res, Err(BankError { .. })));
}

pub async fn test_balance_timeout_returns_error<T: BankAdapterTimeoutTestContext>(context: T) {
    let account = "1";
    let timeout = 1;
    let adapter = context.get_adapter_with_timeout(1).await;
    context.set_latency(timeout + 1).await;

    let res = adapter.balance(account).await;

    assert!(matches!(res, Err(BankError { .. })));
}
