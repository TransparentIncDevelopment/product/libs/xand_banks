use crate::banks::config::BankConfigurationErrors;
use crate::banks::errors::BankErrors;
use snafu::Snafu;

#[derive(Debug, Snafu, Clone, Serialize)]
#[snafu(visibility(pub(crate)))]
pub enum XandBanksErrors {
    #[snafu(display("{}", source))]
    ConfigurationError {
        source: BankConfigurationErrors,
    },
    #[snafu(display("{}", source))]
    BankError {
        source: BankErrors,
    },
    #[snafu(display("Error ({})", message))]
    General {
        message: String,
    },
    Mock,
    #[snafu(display("Bank not found by routing number: ({})", routing_number))]
    BankNotFound {
        routing_number: String,
    },
    #[snafu(display("Account not found for account id: ({})", account_id))]
    AccountNotFound {
        account_id: String,
    },
}

impl From<BankErrors> for XandBanksErrors {
    fn from(source: BankErrors) -> XandBanksErrors {
        XandBanksErrors::BankError { source }
    }
}

impl From<BankConfigurationErrors> for XandBanksErrors {
    fn from(source: BankConfigurationErrors) -> XandBanksErrors {
        XandBanksErrors::ConfigurationError { source }
    }
}
