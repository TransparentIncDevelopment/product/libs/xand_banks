
#See published images here: https://console.cloud.google.com/gcr/images/xand-dev/global/xand-bank-mocks?authuser=1&project=xand-dev
export BANK_MOCKS_VERSION := "gcr.io/xand-dev/xand-bank-mocks:5.2.0"

integ-tests:
	docker pull ${BANK_MOCKS_VERSION}
	cargo test --features integ --test integ
