use crate::tests::docker::create_docker_connection;
use bollard::container::{Config, InspectContainerOptions};
use bollard::models::HostConfig;
use curl::easy::Easy;
use std::env;
use std::io::Read;
use std::ops::Add;
use std::thread::sleep;
use std::time::{Duration, SystemTime};
use url::Url;

pub mod error {
    use std::env::VarError;
    use thiserror::Error;

    #[derive(Debug, Error)]
    pub enum Error {
        #[error(transparent)]
        DockerClient(#[from] crate::tests::docker::error::Error),
        #[error(transparent)]
        Container(#[from] bollard::errors::Error),
        #[error(transparent)]
        Var(#[from] VarError),
    }
}

pub struct BankMocksContainer {
    port: String,
    container_id: String,
}

impl BankMocksContainer {
    pub async fn start_on_ephemeral() -> Result<Self, error::Error> {
        let docker = create_docker_connection().await.unwrap();
        let raw_container_version = env::var("BANK_MOCKS_VERSION")?;
        //remove quotes
        let container_version = &raw_container_version[1..raw_container_version.len() - 1];
        dbg!(&container_version);
        let config = Config {
            image: Some(container_version),
            host_config: Some(HostConfig {
                publish_all_ports: Some(true),
                ..Default::default()
            }),
            ..Default::default()
        };
        let container = docker
            // Passing `None` as first arg results in random container name
            .create_container::<String, _>(None, config)
            .await?;
        docker
            .start_container::<String>(&container.id, None)
            .await?;

        let inspected_container = docker
            .inspect_container(&container.id, Some(InspectContainerOptions { size: false }))
            .await?;
        let container_port = inspected_container
            .network_settings
            .and_then(|ns| ns.ports)
            .and_then(|p| p.get("8888/tcp").cloned().unwrap_or(None))
            .and_then(|vec_port_binding| vec_port_binding.get(0).cloned())
            .and_then(|zeroth_elem| zeroth_elem.host_port)
            .expect("Could not get container port binding");

        dbg!(&container_port);
        Ok(BankMocksContainer {
            port: container_port,
            container_id: container.id,
        })
    }

    //Poll the health endpoint and returns when the service is health or timeout is reached
    pub async fn wait_until_healthy(&self) {
        let timeout = SystemTime::now().add(Duration::from_secs(10));
        while SystemTime::now() < timeout {
            let response = reqwest::get(format!("http://localhost:{}/health", &self.port)).await;
            if let Ok(_res) = response {
                return;
            }
            sleep(Duration::from_millis(10));
        }
        panic!("Container never became healthy")
    }

    pub fn mcb_url(&self) -> Url {
        let url = format!("http://localhost:{}/metropolitan", &self.port);
        url.parse().unwrap()
    }

    pub(crate) fn treasury_prime_url(&self) -> Url {
        let url = format!("http://localhost:{}/provident", &self.port);
        url.parse().unwrap()
    }

    fn latency_url(&self) -> String {
        format!("http://localhost:{}/latency", &self.port)
    }

    pub(crate) async fn add_latency(&self, seconds: u64) {
        let body = format!("seconds={}", seconds);
        let mut data = (body).as_bytes();

        let mut easy = Easy::new();
        easy.url(&self.latency_url()).unwrap();
        easy.post(true).unwrap();
        easy.post_field_size(data.len() as u64).unwrap();

        let mut transfer = easy.transfer();
        transfer
            .read_function(|buf| Ok(data.read(buf).unwrap_or(0)))
            .unwrap();
        transfer.perform().unwrap()
    }
}

impl Drop for BankMocksContainer {
    fn drop(&mut self) {
        let container_id = self.container_id.clone();
        let (sender, receiver) = std::sync::mpsc::channel();

        std::thread::spawn(move || {
            let new_rt = tokio::runtime::Runtime::new().unwrap();
            new_rt.block_on(async {
                let docker = create_docker_connection().await.unwrap();
                let stop_res = docker.stop_container(&container_id, None).await;
                sender.send(stop_res).unwrap();
            });
        });
        let timeout = Duration::from_secs(10);
        let stop_res = receiver.recv_timeout(timeout);
        match stop_res {
            Ok(_) => {
                println!("Stopped container: {:?}", &self.container_id)
            }
            Err(err) => {
                println!("Error while stopping trustee container: {:?}", err)
            }
        }
    }
}
